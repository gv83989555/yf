package com.yufeng.yf.admin.mapper;

import com.yufeng.yf.admin.pojo.AdminOrganization;
import com.yufeng.yf.admin.pojo.AdminOrganizationExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AdminOrganizationMapper {
    int countByExample(AdminOrganizationExample example);

    int deleteByExample(AdminOrganizationExample example);

    int deleteByPrimaryKey(Integer organizationId);

    int insert(AdminOrganization record);

    int insertSelective(AdminOrganization record);

    List<AdminOrganization> selectByExample(AdminOrganizationExample example);

    AdminOrganization selectByPrimaryKey(Integer organizationId);

    int updateByExampleSelective(@Param("record") AdminOrganization record, @Param("example") AdminOrganizationExample example);

    int updateByExample(@Param("record") AdminOrganization record, @Param("example") AdminOrganizationExample example);

    int updateByPrimaryKeySelective(AdminOrganization record);

    int updateByPrimaryKey(AdminOrganization record);
}