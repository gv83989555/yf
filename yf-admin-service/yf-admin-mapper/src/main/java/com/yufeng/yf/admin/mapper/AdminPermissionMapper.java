package com.yufeng.yf.admin.mapper;

import com.yufeng.yf.admin.pojo.AdminPermission;
import com.yufeng.yf.admin.pojo.AdminPermissionExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AdminPermissionMapper {
    int countByExample(AdminPermissionExample example);

    int deleteByExample(AdminPermissionExample example);

    int deleteByPrimaryKey(Integer permissionId);

    int insert(AdminPermission record);

    int insertSelective(AdminPermission record);

    List<AdminPermission> selectByExample(AdminPermissionExample example);

    AdminPermission selectByPrimaryKey(Integer permissionId);

    int updateByExampleSelective(@Param("record") AdminPermission record, @Param("example") AdminPermissionExample example);

    int updateByExample(@Param("record") AdminPermission record, @Param("example") AdminPermissionExample example);

    int updateByPrimaryKeySelective(AdminPermission record);

    int updateByPrimaryKey(AdminPermission record);
}