package com.yufeng.yf.admin.mapper;

import com.yufeng.yf.admin.pojo.AdminRolePermission;
import com.yufeng.yf.admin.pojo.AdminRolePermissionExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AdminRolePermissionMapper {
    int countByExample(AdminRolePermissionExample example);

    int deleteByExample(AdminRolePermissionExample example);

    int deleteByPrimaryKey(Integer rolePermissionId);

    int insert(AdminRolePermission record);

    int insertSelective(AdminRolePermission record);

    List<AdminRolePermission> selectByExample(AdminRolePermissionExample example);

    AdminRolePermission selectByPrimaryKey(Integer rolePermissionId);

    int updateByExampleSelective(@Param("record") AdminRolePermission record, @Param("example") AdminRolePermissionExample example);

    int updateByExample(@Param("record") AdminRolePermission record, @Param("example") AdminRolePermissionExample example);

    int updateByPrimaryKeySelective(AdminRolePermission record);

    int updateByPrimaryKey(AdminRolePermission record);
}