package com.yufeng.yf.admin.mapper;

import com.yufeng.yf.admin.pojo.AdminSystem;
import com.yufeng.yf.admin.pojo.AdminSystemExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AdminSystemMapper {
    int countByExample(AdminSystemExample example);

    int deleteByExample(AdminSystemExample example);

    int deleteByPrimaryKey(Integer systemId);

    int insert(AdminSystem record);

    int insertSelective(AdminSystem record);

    List<AdminSystem> selectByExample(AdminSystemExample example);

    AdminSystem selectByPrimaryKey(Integer systemId);

    int updateByExampleSelective(@Param("record") AdminSystem record, @Param("example") AdminSystemExample example);

    int updateByExample(@Param("record") AdminSystem record, @Param("example") AdminSystemExample example);

    int updateByPrimaryKeySelective(AdminSystem record);

    int updateByPrimaryKey(AdminSystem record);
}