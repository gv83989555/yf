package com.yufeng.yf.admin.mapper;

import com.yufeng.yf.admin.pojo.AdminUserOrganization;
import com.yufeng.yf.admin.pojo.AdminUserOrganizationExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AdminUserOrganizationMapper {
    int countByExample(AdminUserOrganizationExample example);

    int deleteByExample(AdminUserOrganizationExample example);

    int deleteByPrimaryKey(Integer userOrganizationId);

    int insert(AdminUserOrganization record);

    int insertSelective(AdminUserOrganization record);

    List<AdminUserOrganization> selectByExample(AdminUserOrganizationExample example);

    AdminUserOrganization selectByPrimaryKey(Integer userOrganizationId);

    int updateByExampleSelective(@Param("record") AdminUserOrganization record, @Param("example") AdminUserOrganizationExample example);

    int updateByExample(@Param("record") AdminUserOrganization record, @Param("example") AdminUserOrganizationExample example);

    int updateByPrimaryKeySelective(AdminUserOrganization record);

    int updateByPrimaryKey(AdminUserOrganization record);
}