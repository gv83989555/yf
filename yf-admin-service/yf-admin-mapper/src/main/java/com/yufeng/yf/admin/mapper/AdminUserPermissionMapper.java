package com.yufeng.yf.admin.mapper;

import com.yufeng.yf.admin.pojo.AdminUserPermission;
import com.yufeng.yf.admin.pojo.AdminUserPermissionExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AdminUserPermissionMapper {
    int countByExample(AdminUserPermissionExample example);

    int deleteByExample(AdminUserPermissionExample example);

    int deleteByPrimaryKey(Integer userPermissionId);

    int insert(AdminUserPermission record);

    int insertSelective(AdminUserPermission record);

    List<AdminUserPermission> selectByExample(AdminUserPermissionExample example);

    AdminUserPermission selectByPrimaryKey(Integer userPermissionId);

    int updateByExampleSelective(@Param("record") AdminUserPermission record, @Param("example") AdminUserPermissionExample example);

    int updateByExample(@Param("record") AdminUserPermission record, @Param("example") AdminUserPermissionExample example);

    int updateByPrimaryKeySelective(AdminUserPermission record);

    int updateByPrimaryKey(AdminUserPermission record);
}