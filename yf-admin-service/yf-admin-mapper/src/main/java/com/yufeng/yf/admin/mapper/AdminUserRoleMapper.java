package com.yufeng.yf.admin.mapper;

import com.yufeng.yf.admin.pojo.AdminUserRole;
import com.yufeng.yf.admin.pojo.AdminUserRoleExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AdminUserRoleMapper {
    int countByExample(AdminUserRoleExample example);

    int deleteByExample(AdminUserRoleExample example);

    int deleteByPrimaryKey(Integer userRoleId);

    int insert(AdminUserRole record);

    int insertSelective(AdminUserRole record);

    List<AdminUserRole> selectByExample(AdminUserRoleExample example);

    AdminUserRole selectByPrimaryKey(Integer userRoleId);

    int updateByExampleSelective(@Param("record") AdminUserRole record, @Param("example") AdminUserRoleExample example);

    int updateByExample(@Param("record") AdminUserRole record, @Param("example") AdminUserRoleExample example);

    int updateByPrimaryKeySelective(AdminUserRole record);

    int updateByPrimaryKey(AdminUserRole record);
}