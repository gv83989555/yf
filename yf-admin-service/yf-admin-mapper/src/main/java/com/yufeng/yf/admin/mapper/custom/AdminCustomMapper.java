package com.yufeng.yf.admin.mapper.custom;

import com.yufeng.yf.admin.pojo.AdminPermission;
import com.yufeng.yf.admin.pojo.AdminRole;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author 高巍
 * @createTime 2018年09月03日 11:48
 * @description 后台自定义mapper
 */
public interface AdminCustomMapper {

    /**
     * 根据用户id获取所属的角色
     * @param userId
     * @return
     */
    @Select("select " +
            "role_id, name, title, description, ctime, orders " +
            "from admin_role ar where ar.role_id in ( " +
            "select aur.role_id from admin_user_role aur where aur.user_id=#{userId,jdbcType=INTEGER} " +
            ")")
    List<AdminRole> selectAdminRoleByAdminUserId(@Param("userId") Integer userId);

    /**
     * 根据用户获取所拥有的权限（角色关联的权限+用户关联的权限）
     * @param adminUserId
     * @return
     */
    @Select("select " +
            "permission_id, system_id, pid, name, type, permission_value, uri, icon, status, ctime, orders " +
            "from admin_permission up where up.`status`=1 and up.permission_id in ( " +
            "select permission_id from admin_role_permission urp where urp.role_id in ( " +
            "select uur.role_id role_id from admin_user_role uur where uur.user_id=#{adminUserId,jdbcType=INTEGER} " +
            ") " +
            "union " +
            "select permission_id from admin_user_permission uup1 where uup1.user_id=#{adminUserId,jdbcType=INTEGER} and uup1.type=1 " +
            ") " +
            "and up.permission_id not in ( " +
            "select permission_id from admin_user_permission uup2 where uup2.user_id=#{adminUserId,jdbcType=INTEGER} and uup2.type=-1 " +
            ") order by up.orders asc")
    List<AdminPermission> selectAdminPermissionByAdminUserId(@Param("adminUserId") Integer adminUserId);


}
