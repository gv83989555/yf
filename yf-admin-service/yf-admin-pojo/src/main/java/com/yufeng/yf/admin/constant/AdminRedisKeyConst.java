package com.yufeng.yf.admin.constant;

/**
 * @author GV
 * Date:2018/5/13
 * Time:下午4:16
 */

public class AdminRedisKeyConst {

    /**
     * shiro会话key
     */
    public final static String ADMIN_SHIRO_SESSION_ID = "ADMIN:SHIRO-SESSION-ID_";

    /**
     * session被强退标记
     */
    public final static String FORCE_LOGOUT = "FORCE_LOGOUT";

    /**
     * 全局会话列表key
     * 全部SSO登录成功的会话
     */
    public final static String ADMIN_SERVER_SESSION_IDS = "ADMIN:SERVER_SESSION_IDS";

    /**
     * 系统消息模版
     */
    public final static String SYSTEM_INFORM_TEMPLATE_HASH = "ADMIN:SYSTEM_INFORM_TEMPLATE_HASH";


    /**
     * 系统消息 发送队列
     */
    public final static String SYSTEM_INFORM_SEND_QUEUE = "ADMIN:SYSTEM_INFORM_SEND_QUEUE";


}
