package com.yufeng.yf.admin.constant;

/**
 * @author GV
 * Date:2018/5/11
 * Time:下午1:20
 */

public class AdminUserConstant {

    /**
     * 用户状态
     */
    public static final byte STATE_LOCKED = 0;
    public static final byte STATE_NORMAL = 1;

    /**
     * 用户性别
     */
    public static final byte NOT_CHOOSE = 0;
    public static final byte BOY = 1;
    public static final byte GIRL = 2;

}
