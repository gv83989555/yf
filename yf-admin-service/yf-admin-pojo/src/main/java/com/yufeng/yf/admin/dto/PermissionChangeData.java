package com.yufeng.yf.admin.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author GV
 * Date:2018/3/21
 * Time:上午11:01
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PermissionChangeData implements Serializable {
    private Integer id;
    private Boolean checked;
    private Byte type;
}
