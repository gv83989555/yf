package com.yufeng.yf.admin.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @author GV
 * Date:2018/3/20
 * Time:上午6:52
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ZtreeVO implements Serializable{
    private Integer id;
    private String name;
    private Boolean nocheck;
    private Boolean checked;
    private Boolean open;
    private List<ZtreeVO> children;

}
