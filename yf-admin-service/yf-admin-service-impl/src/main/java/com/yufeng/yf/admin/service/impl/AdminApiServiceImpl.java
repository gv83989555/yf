package com.yufeng.yf.admin.service.impl;

import com.yufeng.yf.admin.mapper.*;
import com.yufeng.yf.admin.mapper.custom.AdminCustomMapper;
import com.yufeng.yf.admin.pojo.*;
import com.yufeng.yf.admin.service.AdminApiService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;

/**
 * @author GV
 * Date:2018/2/13
 * Time:下午3:07
 */

@org.springframework.stereotype.Service
public class AdminApiServiceImpl implements AdminApiService {
    private static Logger logger = LoggerFactory.getLogger(AdminApiServiceImpl.class);

    @Autowired
    AdminUserMapper adminUserMapper;

    @Autowired
    AdminCustomMapper adminApiMapper;

    @Autowired
    AdminRolePermissionMapper adminRolePermissionMapper;

    @Autowired
    AdminUserPermissionMapper adminUserPermissionMapper;

    @Autowired
    AdminSystemMapper adminSystemMapper;

    @Autowired
    AdminOrganizationMapper adminOrganizationMapper;

    @Autowired
    AdminLogMapper adminLogMapper;

    /**
     * 根据用户id获取所拥有的权限
     *
     * @param adminUserId
     * @return
     */
    @Override
    public List<AdminPermission> selectAdminPermissionByAdminUserId(Integer adminUserId) {
        // 用户不存在或锁定状态
//        AdminUser adminUser = adminUserMapper.selectByPrimaryKey(adminUserId);
//        if (null == adminUser || 0 == adminUser.getLocked()) {
//            logger.info("selectAdminPermissionByAdminUserId : AdminUserId={}", adminUserId);
//            return null;
//        }
        return adminApiMapper.selectAdminPermissionByAdminUserId(adminUserId);
    }

    /**
     * 根据用户id获取所拥有的权限
     *
     * @param adminUserId
     * @return
     */
    @Override
    @Cacheable(value = "Admin-rpc-service-ehcache", key = "'selectAdminPermissionByAdminUserId_' + #adminUserId")
    public List<AdminPermission> selectAdminPermissionByAdminUserIdByCache(Integer adminUserId) {
        return selectAdminPermissionByAdminUserId(adminUserId);
    }

    /**
     * 根据用户id获取所属的角色
     *
     * @param adminUserId
     * @return
     */
    @Override
    public List<AdminRole> selectAdminRoleByAdminUserId(Integer adminUserId) {
        // 用户不存在或锁定状态
//        AdminUser adminUser = adminUserMapper.selectByPrimaryKey(adminUserId);
//        if (null == adminUser || 0 == adminUser.getLocked()) {
//            logger.info(" 找不到用户selectAdminRoleByAdminUserId : AdminUserId={}", adminUserId);
//            return null;
//        }
        List<AdminRole> adminRoles = adminApiMapper.selectAdminRoleByAdminUserId(adminUserId);
        return adminRoles;
    }

    /**
     * 根据用户id获取所属的角色
     *
     * @param adminUserId
     * @return
     */
    @Override
    @Cacheable(value = "zheng-Admin-rpc-service-ehcache", key = "'selectAdminRoleByAdminUserId_' + #adminUserId")
    public List<AdminRole> selectAdminRoleByAdminUserIdByCache(Integer adminUserId) {
        return selectAdminRoleByAdminUserId(adminUserId);
    }

    /**
     * 根据角色id获取所拥有的权限
     *
     * @param adminRoleId
     * @return
     */
    @Override
    public List<AdminRolePermission> selectAdminRolePermisstionByAdminRoleId(Integer adminRoleId) {
        AdminRolePermissionExample adminRolePermissionExample = new AdminRolePermissionExample();
        adminRolePermissionExample.createCriteria()
                .andRoleIdEqualTo(adminRoleId);
        List<AdminRolePermission> AdminRolePermissions = adminRolePermissionMapper.selectByExample(adminRolePermissionExample);
        return AdminRolePermissions;
    }

    /**
     * 根据用户id获取所拥有的权限
     *
     * @param adminUserId
     * @return
     */
    @Override
    public List<AdminUserPermission> selectAdminUserPermissionByAdminUserId(Integer adminUserId) {
        AdminUserPermissionExample adminUserPermissionExample = new AdminUserPermissionExample();
        adminUserPermissionExample.createCriteria()
                .andUserIdEqualTo(adminUserId);
        List<AdminUserPermission> adminUserPermissions = adminUserPermissionMapper.selectByExample(adminUserPermissionExample);
        return adminUserPermissions;
    }

    /**
     * 根据条件获取系统数据
     *
     * @param adminSystemExample
     * @return
     */
    @Override
    public List<AdminSystem> selectAdminSystemByExample(AdminSystemExample adminSystemExample) {
        return adminSystemMapper.selectByExample(adminSystemExample);
    }

    /**
     * 根据条件获取组织数据
     *
     * @param adminOrganizationExample
     * @return
     */
    @Override
    public List<AdminOrganization> selectAdminOrganizationByExample(AdminOrganizationExample adminOrganizationExample) {
        return adminOrganizationMapper.selectByExample(adminOrganizationExample);
    }

    /**
     * 根据username获取AdminUser
     *
     * @param username
     * @return
     */
    @Override
    public AdminUser selectAdminUserByUsername(String username) {
        AdminUserExample adminUserExample = new AdminUserExample();
        adminUserExample.createCriteria()
                .andUsernameEqualTo(username);
        List<AdminUser> adminUsers = adminUserMapper.selectByExample(adminUserExample);
        if (null != adminUsers && adminUsers.size() > 0) {
            return adminUsers.get(0);
        }
        return null;
    }

    /**
     * 写入操作日志
     *
     * @param record
     * @return
     */
    @Override
    public int insertAdminLogSelective(AdminLog record) {
        return adminLogMapper.insertSelective(record);
    }

}
