package com.yufeng.yf.admin.service.impl;

import com.yufeng.yf.admin.mapper.AdminLogMapper;
import com.yufeng.yf.admin.pojo.AdminLog;
import com.yufeng.yf.admin.pojo.AdminLogExample;
import com.yufeng.yf.admin.service.AdminLogService;
import com.yufeng.yf.common.service.annotation.BaseService;
import com.yufeng.yf.common.service.base.BaseServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
* @author GV
* on 2018/05/13 10:25:03
* AdminLogService接口实现类
*/

@org.springframework.stereotype.Service
@BaseService
public class AdminLogServiceImpl extends BaseServiceImpl<AdminLogMapper, AdminLog, AdminLogExample> implements AdminLogService {

    private static Logger LOGGER = LoggerFactory.getLogger(AdminLogServiceImpl.class);

    @Autowired
    private AdminLogMapper adminLogMapper;

}