package com.yufeng.yf.admin.service.impl;

import com.yufeng.yf.admin.mapper.AdminOrganizationMapper;
import com.yufeng.yf.admin.pojo.AdminOrganization;
import com.yufeng.yf.admin.pojo.AdminOrganizationExample;
import com.yufeng.yf.admin.service.AdminOrganizationService;
import com.yufeng.yf.common.service.annotation.BaseService;
import com.yufeng.yf.common.service.base.BaseServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
* @author GV
* on 2018/05/13 10:25:03
* AdminOrganizationService接口实现类
*/

@org.springframework.stereotype.Service
@BaseService
public class AdminOrganizationServiceImpl extends BaseServiceImpl<AdminOrganizationMapper, AdminOrganization, AdminOrganizationExample> implements AdminOrganizationService {

    private static Logger LOGGER = LoggerFactory.getLogger(AdminOrganizationServiceImpl.class);

    @Autowired
    private AdminOrganizationMapper adminOrganizationMapper;

}