package com.yufeng.yf.app.service.impl;

import com.yufeng.yf.admin.mapper.AdminPermissionMapper;
import com.yufeng.yf.admin.mapper.AdminSystemMapper;
import com.yufeng.yf.admin.mapper.AdminUserPermissionMapper;
import com.yufeng.yf.admin.pojo.*;
import com.yufeng.yf.admin.service.AdminApiService;
import com.yufeng.yf.admin.service.AdminPermissionService;
import com.yufeng.yf.admin.vo.ZtreeVO;
import com.yufeng.yf.common.service.annotation.BaseService;
import com.yufeng.yf.common.service.base.BaseServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;
import java.util.stream.Collectors;

/**
* @author GV
* on 2018/05/11 09:21:54
* AdminPermissionService接口实现类
*/

@org.springframework.stereotype.Service
@BaseService
public class AdminPermissionServiceImpl extends BaseServiceImpl<AdminPermissionMapper, AdminPermission, AdminPermissionExample> implements AdminPermissionService {

    private static Logger LOGGER = LoggerFactory.getLogger(AdminPermissionServiceImpl.class);

    @Autowired
    private AdminPermissionMapper adminPermissionMapper;

    @Autowired
    private AdminApiService adminApiService;
    
    @Autowired
    private AdminSystemMapper adminSystemMapper;

    @Autowired
    private AdminUserPermissionMapper adminUserPermissionMapper;

    @Override
    public List<ZtreeVO> getTreeByRoleIdAndSystemId(int roleId, int systemId) {
        //角色拥有权限
        List<AdminRolePermission> rolePermissionList = adminApiService.selectAdminRolePermisstionByAdminRoleId(roleId);
        //权限ID分组，key=权限ID
        Map<Integer, List<AdminRolePermission>> reloPermissoinMap = rolePermissionList.stream().collect(Collectors.groupingBy(AdminRolePermission::getPermissionId));

        List<ZtreeVO> systemNodes = new ArrayList<>();

        //查询指定系统
        AdminSystemExample adminSystemExample = new AdminSystemExample();
        adminSystemExample.createCriteria().andStatusEqualTo((byte) 1).andSystemIdEqualTo(systemId);
        adminSystemExample.setOrderByClause("orders asc");
        List<AdminSystem> systemList = adminSystemMapper.selectByExample(adminSystemExample);

        if (systemList.size() == 0) {
            return systemNodes;
        }
        //获取系统全部权限
        AdminPermissionExample adminPermissionExample = new AdminPermissionExample();
        adminPermissionExample.createCriteria().andStatusEqualTo((byte) 1).andSystemIdEqualTo(systemId);
        adminPermissionExample.setOrderByClause("orders asc");
        List<AdminPermission> allSystemPermissions = adminPermissionMapper.selectByExample(adminPermissionExample);

        AdminSystem adminSystem = systemList.get(0);
        ZtreeVO system = ZtreeVO.builder().id(adminSystem.getSystemId()).name(adminSystem.getTitle()).nocheck(true).children(new ArrayList<>()).open(true).build();
        systemNodes.add(system);
        this.buildSystemZtree(reloPermissoinMap, allSystemPermissions, system);
        return systemNodes;
    }

    @Override
    public List<ZtreeVO> getTreeByUserIdAndSystemId(int id, int systemId, byte type) {
        //获取用户拥有权限
        AdminUserPermissionExample adminUserPermissionExample = new AdminUserPermissionExample();
        adminUserPermissionExample.createCriteria().andUserIdEqualTo(id).andTypeEqualTo(type);
        List<AdminUserPermission> adminUserPermissionList = adminUserPermissionMapper.selectByExample(adminUserPermissionExample);

        //权限ID分组，key=权限ID
        Map<Integer, List<AdminUserPermission>> userPermissionMap = adminUserPermissionList.stream().collect(Collectors.groupingBy(AdminUserPermission::getPermissionId));
        List<ZtreeVO> systemNodes = new ArrayList<>();

        //查询指定系统
        AdminSystemExample adminSystemExample = new AdminSystemExample();
        adminSystemExample.createCriteria().andStatusEqualTo((byte) 1).andSystemIdEqualTo(systemId);
        adminSystemExample.setOrderByClause("orders asc");
        List<AdminSystem> systemList = adminSystemMapper.selectByExample(adminSystemExample);

        if (systemList.size() == 0) {
            return systemNodes;
        }
        //获取系统全部权限
        AdminPermissionExample adminPermissionExample = new AdminPermissionExample();
        adminPermissionExample.createCriteria().andStatusEqualTo((byte) 1).andSystemIdEqualTo(systemId);
        adminPermissionExample.setOrderByClause("orders asc");
        List<AdminPermission> allSystemPermissions = adminPermissionMapper.selectByExample(adminPermissionExample);

        AdminSystem adminSystem = systemList.get(0);

        ZtreeVO system = ZtreeVO.builder().id(adminSystem.getSystemId()).name(adminSystem.getTitle()).nocheck(true).children(new ArrayList<>()).open(true).build();
        systemNodes.add(system);
        buildSystemZtree(userPermissionMap, allSystemPermissions, system);
        return systemNodes;
    }



    /**
     * 构建系统权限树
     */
    private void buildSystemZtree(Map<Integer, ?> permissoinMap, List<AdminPermission> allSystemPermissions, ZtreeVO system) {
        //筛选目录(按排序字段排序)
        List<AdminPermission> foldersPermission = allSystemPermissions.stream().filter(adminPermission -> adminPermission.getType() == 1).sorted(Comparator.comparing(AdminPermission::getOrders)).collect(Collectors.toList());
        foldersPermission.forEach(folderPermission -> {
                    //构建目录权限
                    ZtreeVO folder = ZtreeVO.builder().id(folderPermission.getPermissionId()).name(folderPermission.getName()).children(new ArrayList<>()).open(true).nocheck(false).build();
                    folder.setChecked(permissoinMap.get(folderPermission.getPermissionId()) != null);
                    system.getChildren().add(folder);
                    //筛选当前目录下菜单
                    List<AdminPermission> menusPermission = allSystemPermissions.stream().filter(adminPermission -> (adminPermission.getType() == 2) && (Objects.equals(adminPermission.getPid(), folderPermission.getPermissionId()))).sorted(Comparator.comparing(AdminPermission::getOrders)).collect(Collectors.toList());
                    menusPermission.forEach(menuPermission -> {
                                //构建菜单权限
                                ZtreeVO menu = ZtreeVO.builder().id(menuPermission.getPermissionId()).name(menuPermission.getName()).children(new ArrayList<>()).open(true).nocheck(false).build();
                                menu.setChecked(permissoinMap.get(menuPermission.getPermissionId()) != null);
                                folder.getChildren().add(menu);
                                //筛选当前菜单下按钮
                                List<AdminPermission> buttonsPermission = allSystemPermissions.stream().filter(adminPermission -> (adminPermission.getType() == 3) && (Objects.equals(adminPermission.getPid(), menuPermission.getPermissionId()))).sorted(Comparator.comparing(AdminPermission::getOrders)).collect(Collectors.toList());
                                buttonsPermission.forEach(buttonPermission -> {
                                    //构建按钮权限
                                    ZtreeVO botton = ZtreeVO.builder().id(buttonPermission.getPermissionId()).name(buttonPermission.getName()).children(new ArrayList<>()).open(true).nocheck(false).build();
                                    botton.setChecked(permissoinMap.get(buttonPermission.getPermissionId()) != null);
                                    menu.getChildren().add(botton);
                                });
                            }
                    );
                }
        );
    }
}