package com.yufeng.yf.admin.service.impl;

import com.yufeng.yf.admin.dto.PermissionChangeData;
import com.yufeng.yf.admin.mapper.AdminRolePermissionMapper;
import com.yufeng.yf.admin.pojo.AdminRolePermission;
import com.yufeng.yf.admin.pojo.AdminRolePermissionExample;
import com.yufeng.yf.admin.service.AdminRolePermissionService;
import com.yufeng.yf.common.service.annotation.BaseService;
import com.yufeng.yf.common.service.base.BaseServiceImpl;
import com.yufeng.yf.common.web.entity.ServerResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
* @author GV
* on 2018/05/11 09:21:54
* AdminRolePermissionService接口实现类
*/

@org.springframework.stereotype.Service
@BaseService
public class AdminRolePermissionServiceImpl extends BaseServiceImpl<AdminRolePermissionMapper, AdminRolePermission, AdminRolePermissionExample> implements AdminRolePermissionService {

    private static Logger LOGGER = LoggerFactory.getLogger(AdminRolePermissionServiceImpl.class);

    @Autowired
    private AdminRolePermissionMapper adminRolePermissionMapper;

    @Override
    public ServerResponse rolePermission(PermissionChangeData[] changeDatas, int id) {
        List<Integer> deleteIds = new ArrayList<>();
        for (int i = 0; i < changeDatas.length; i ++) {
            if (!changeDatas[i].getChecked()) {
                deleteIds.add(changeDatas[i].getId());
            } else {
                // 新增权限
                AdminRolePermission adminRolePermission = new AdminRolePermission();
                adminRolePermission.setRoleId(id);
                adminRolePermission.setPermissionId(changeDatas[i].getId());
                adminRolePermissionMapper.insertSelective(adminRolePermission);
            }
        }
        // 删除权限
        if (deleteIds.size() > 0) {
            AdminRolePermissionExample adminRolePermissionExample = new AdminRolePermissionExample();
            adminRolePermissionExample.createCriteria()
                    .andPermissionIdIn(deleteIds)
                    .andRoleIdEqualTo(id);
            adminRolePermissionMapper.deleteByExample(adminRolePermissionExample);
        }
        return ServerResponse.createBySuccess(deleteIds.size());
    }
}