package com.yufeng.yf.admin.service.impl;

import com.yufeng.yf.admin.mapper.AdminRoleMapper;
import com.yufeng.yf.admin.pojo.AdminRole;
import com.yufeng.yf.admin.pojo.AdminRoleExample;
import com.yufeng.yf.admin.service.AdminRoleService;
import com.yufeng.yf.common.service.annotation.BaseService;
import com.yufeng.yf.common.service.base.BaseServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
* @author GV
* on 2018/05/11 09:21:54
* AdminRoleService接口实现类
*/

@org.springframework.stereotype.Service
@BaseService
public class AdminRoleServiceImpl extends BaseServiceImpl<AdminRoleMapper, AdminRole, AdminRoleExample> implements AdminRoleService {

    private static Logger LOGGER = LoggerFactory.getLogger(AdminRoleServiceImpl.class);

    @Autowired
    private AdminRoleMapper adminRoleMapper;


}