package com.yufeng.yf.admin.service.impl;

import com.yufeng.yf.admin.mapper.AdminSystemMapper;
import com.yufeng.yf.admin.pojo.AdminSystem;
import com.yufeng.yf.admin.pojo.AdminSystemExample;
import com.yufeng.yf.admin.service.AdminSystemService;
import com.yufeng.yf.common.service.annotation.BaseService;
import com.yufeng.yf.common.service.base.BaseServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
* @author GV
* on 2018/05/13 10:25:03
* AdminSystemService接口实现类
*/

@org.springframework.stereotype.Service
@BaseService
public class AdminSystemServiceImpl extends BaseServiceImpl<AdminSystemMapper, AdminSystem, AdminSystemExample> implements AdminSystemService {

    private static Logger LOGGER = LoggerFactory.getLogger(AdminSystemServiceImpl.class);

    @Autowired
    private AdminSystemMapper adminSystemMapper;

}