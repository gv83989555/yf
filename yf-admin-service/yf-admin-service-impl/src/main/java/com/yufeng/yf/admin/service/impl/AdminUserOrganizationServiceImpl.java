package com.yufeng.yf.admin.service.impl;

import com.yufeng.yf.admin.mapper.AdminUserOrganizationMapper;
import com.yufeng.yf.admin.pojo.AdminUserOrganization;
import com.yufeng.yf.admin.pojo.AdminUserOrganizationExample;
import com.yufeng.yf.admin.service.AdminUserOrganizationService;
import com.yufeng.yf.common.service.annotation.BaseService;
import com.yufeng.yf.common.service.base.BaseServiceImpl;
import com.yufeng.yf.common.web.entity.ServerResponse;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;

/**
* @author GV
* on 2018/05/13 10:25:03
* AdminUserOrganizationService接口实现类
*/

@org.springframework.stereotype.Service
@BaseService
public class AdminUserOrganizationServiceImpl extends BaseServiceImpl<AdminUserOrganizationMapper, AdminUserOrganization, AdminUserOrganizationExample> implements AdminUserOrganizationService {

    private static Logger LOGGER = LoggerFactory.getLogger(AdminUserOrganizationServiceImpl.class);

    @Autowired
    private AdminUserOrganizationMapper adminUserOrganizationMapper;

    @Override
    public ServerResponse organization(int id, String[] organizationIds) {
        //删除原有记录
        AdminUserOrganizationExample upmsUserOrganizationExample=new AdminUserOrganizationExample();
        upmsUserOrganizationExample.createCriteria().andUserIdEqualTo(id);
        super.deleteByExample(upmsUserOrganizationExample);
        if (null==organizationIds||organizationIds.length==0){
            return ServerResponse.createBySuccess();
        }
        //重新绑定
        Arrays.stream(organizationIds).forEach(i ->{
            if (StringUtils.isNotBlank(i)){
                AdminUserOrganization adminUserOrganization=new AdminUserOrganization();
                adminUserOrganization.setUserId(id);
                adminUserOrganization.setOrganizationId(Integer.parseInt(i));
                super.insertSelective(adminUserOrganization);
            }
        });
        return ServerResponse.createBySuccess(1);
    }
}