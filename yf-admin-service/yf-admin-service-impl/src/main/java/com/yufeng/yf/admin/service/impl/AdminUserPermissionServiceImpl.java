package com.yufeng.yf.admin.service.impl;

import com.yufeng.yf.admin.dto.PermissionChangeData;
import com.yufeng.yf.admin.mapper.AdminUserPermissionMapper;
import com.yufeng.yf.admin.pojo.AdminUserPermission;
import com.yufeng.yf.admin.pojo.AdminUserPermissionExample;
import com.yufeng.yf.admin.service.AdminUserPermissionService;
import com.yufeng.yf.common.service.annotation.BaseService;
import com.yufeng.yf.common.service.base.BaseServiceImpl;
import com.yufeng.yf.common.web.entity.ServerResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
* @author GV
* on 2018/05/13 14:38:35
* AdminUserPermissionService接口实现类
*/

@org.springframework.stereotype.Service
@BaseService
public class AdminUserPermissionServiceImpl extends BaseServiceImpl<AdminUserPermissionMapper, AdminUserPermission, AdminUserPermissionExample> implements AdminUserPermissionService {

    private static Logger LOGGER = LoggerFactory.getLogger(AdminUserPermissionServiceImpl.class);

    @Autowired
    private AdminUserPermissionMapper adminUserPermissionMapper;

    @Override
    public ServerResponse permission(PermissionChangeData[] changeDatas, int id) {
        for (int i = 0; i < changeDatas.length; i ++) {
            if (changeDatas[i].getChecked()) {
                // 新增权限
                AdminUserPermission upmsUserPermission = new AdminUserPermission();
                upmsUserPermission.setUserId(id);
                upmsUserPermission.setPermissionId(changeDatas[i].getId());
                upmsUserPermission.setType(changeDatas[i].getType());
                adminUserPermissionMapper.insertSelective(upmsUserPermission);
            } else {
                // 删除权限
                AdminUserPermissionExample adminUserPermissionExample = new AdminUserPermissionExample();
                adminUserPermissionExample.createCriteria()
                        .andPermissionIdEqualTo(changeDatas[i].getId())
                        .andTypeEqualTo(changeDatas[i].getType());
                adminUserPermissionMapper.deleteByExample(adminUserPermissionExample);
            }
        }
        return ServerResponse.createBySuccess();
    }
}