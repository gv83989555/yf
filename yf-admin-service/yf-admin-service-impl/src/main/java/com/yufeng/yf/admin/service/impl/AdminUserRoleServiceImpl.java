package com.yufeng.yf.admin.service.impl;

import com.yufeng.yf.admin.mapper.AdminUserRoleMapper;
import com.yufeng.yf.admin.pojo.AdminUserRole;
import com.yufeng.yf.admin.pojo.AdminUserRoleExample;
import com.yufeng.yf.admin.service.AdminUserRoleService;
import com.yufeng.yf.common.service.annotation.BaseService;
import com.yufeng.yf.common.service.base.BaseServiceImpl;
import com.yufeng.yf.common.web.entity.ServerResponse;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
* @author GV
* on 2018/05/11 09:21:54
* AdminUserRoleService接口实现类
*/

@org.springframework.stereotype.Service
@BaseService
public class AdminUserRoleServiceImpl extends BaseServiceImpl<AdminUserRoleMapper, AdminUserRole, AdminUserRoleExample> implements AdminUserRoleService {

    private static Logger LOGGER = LoggerFactory.getLogger(AdminUserRoleServiceImpl.class);

    @Autowired
    private AdminUserRoleMapper adminUserRoleMapper;

    @Override
    public ServerResponse role(String[] roleIds, int id) {
        int result = 0;
        // 删除旧记录
        AdminUserRoleExample upmsUserRoleExample = new AdminUserRoleExample();
        upmsUserRoleExample.createCriteria()
                .andUserIdEqualTo(id);
        adminUserRoleMapper.deleteByExample(upmsUserRoleExample);
        // 增加新记录
        if (null != roleIds) {
            for (String roleId : roleIds) {
                if (StringUtils.isBlank(roleId)) {
                    continue;
                }
                AdminUserRole adminUserRole = new AdminUserRole();
                adminUserRole.setUserId(id);
                adminUserRole.setRoleId(NumberUtils.toInt(roleId));
                result = adminUserRoleMapper.insertSelective(adminUserRole);
            }
        }
        return ServerResponse.createBySuccess(result);
    }
}