package com.yufeng.yf.admin.service.impl;

import com.yufeng.yf.admin.mapper.AdminUserMapper;
import com.yufeng.yf.admin.mapper.custom.AdminCustomMapper;
import com.yufeng.yf.admin.pojo.AdminUser;
import com.yufeng.yf.admin.pojo.AdminUserExample;
import com.yufeng.yf.admin.service.AdminUserService;
import com.yufeng.yf.common.service.annotation.BaseService;
import com.yufeng.yf.common.service.base.BaseServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
* @author GV
* on 2018/05/11 09:21:54
* AdminUserService接口实现类
*/

@org.springframework.stereotype.Service
@BaseService
public class AdminUserServiceImpl extends BaseServiceImpl<AdminUserMapper, AdminUser, AdminUserExample> implements AdminUserService {

    private static Logger LOGGER = LoggerFactory.getLogger(AdminUserServiceImpl.class);

    @Autowired
    private AdminUserMapper adminUserMapper;


    @Autowired
    private AdminCustomMapper adminApiMapper;




}