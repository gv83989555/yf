package com.yufeng.yf.admin.service;


import com.yufeng.yf.admin.pojo.*;

import java.util.List;

/**
 * admin系统接口
 * @author gv
 */
public interface AdminApiService {

    /**
     * 根据用户id获取所拥有的权限(用户和角色权限合集)
     * @param adminUserId
     * @return
     */
    List<AdminPermission> selectAdminPermissionByAdminUserId(Integer adminUserId);

    /**
     * 根据用户id获取所拥有的权限(用户和角色权限合集)
     * @param adminUserId
     * @return
     */
    List<AdminPermission> selectAdminPermissionByAdminUserIdByCache(Integer adminUserId);

    /**
     * 根据用户id获取所属的角色
     * @param adminUserId
     * @return
     */
    List<AdminRole> selectAdminRoleByAdminUserId(Integer adminUserId);

    /**
     * 根据用户id获取所属的角色
     * @param adminUserId
     * @return
     */
    List<AdminRole> selectAdminRoleByAdminUserIdByCache(Integer adminUserId);

    /**
     * 根据角色id获取所拥有的权限
     * @param adminRoleId
     * @return
     */
    List<AdminRolePermission> selectAdminRolePermisstionByAdminRoleId(Integer adminRoleId);

    /**
     * 根据用户id获取所拥有的权限
     * @param adminUserId
     * @return
     */
    List<AdminUserPermission> selectAdminUserPermissionByAdminUserId(Integer adminUserId);

    /**
     * 根据条件获取系统数据
     * @param adminSystemExample
     * @return
     */
    List<AdminSystem> selectAdminSystemByExample(AdminSystemExample adminSystemExample);

    /**
     * 根据条件获取组织数据
     * @param adminOrganizationExample
     * @return
     */
    List<AdminOrganization> selectAdminOrganizationByExample(AdminOrganizationExample adminOrganizationExample);

    /**
     * 根据username获取AdminUser
     * @param username
     * @return
     */
    AdminUser selectAdminUserByUsername(String username);

    /**
     * 写入操作日志
     * @param record
     * @return
     */
    int insertAdminLogSelective(AdminLog record);

}
