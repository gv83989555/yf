package com.yufeng.yf.admin.service;

import com.yufeng.yf.admin.pojo.AdminLog;
import com.yufeng.yf.admin.pojo.AdminLogExample;
import com.yufeng.yf.common.service.base.BaseService;


/**
* @author GV
* on 2018/05/13 10:25:03
* AdminLogService接口
*/
public interface AdminLogService extends BaseService<AdminLog, AdminLogExample> {

}