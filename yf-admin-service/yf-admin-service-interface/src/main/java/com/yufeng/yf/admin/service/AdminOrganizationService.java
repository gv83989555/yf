package com.yufeng.yf.admin.service;

import com.yufeng.yf.admin.pojo.AdminOrganization;
import com.yufeng.yf.admin.pojo.AdminOrganizationExample;
import com.yufeng.yf.common.service.base.BaseService;


/**
* @author GV
* on 2018/05/13 10:25:03
* AdminOrganizationService接口
*/
public interface AdminOrganizationService extends BaseService<AdminOrganization, AdminOrganizationExample> {

}