package com.yufeng.yf.admin.service;

import com.yufeng.yf.admin.pojo.AdminPermission;
import com.yufeng.yf.admin.pojo.AdminPermissionExample;
import com.yufeng.yf.admin.vo.ZtreeVO;
import com.yufeng.yf.common.service.base.BaseService;

import java.util.List;


/**
* @author GV
* on 2018/05/11 09:21:54
* AdminPermissionService接口
*/
public interface AdminPermissionService extends BaseService<AdminPermission, AdminPermissionExample> {

    List<ZtreeVO> getTreeByRoleIdAndSystemId(int roleId, int systemId);

    List<ZtreeVO>  getTreeByUserIdAndSystemId(int id, int systemId, byte type);
}