package com.yufeng.yf.admin.service;

import com.yufeng.yf.admin.dto.PermissionChangeData;
import com.yufeng.yf.admin.pojo.AdminRolePermission;
import com.yufeng.yf.admin.pojo.AdminRolePermissionExample;
import com.yufeng.yf.common.service.base.BaseService;
import com.yufeng.yf.common.web.entity.ServerResponse;


/**
* @author GV
* on 2018/05/11 09:21:54
* AdminRolePermissionService接口
*/
public interface AdminRolePermissionService extends BaseService<AdminRolePermission, AdminRolePermissionExample> {
    ServerResponse rolePermission(PermissionChangeData[] changeDatas, int id);
}