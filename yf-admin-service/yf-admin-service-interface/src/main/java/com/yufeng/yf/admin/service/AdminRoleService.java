package com.yufeng.yf.admin.service;

import com.yufeng.yf.admin.pojo.AdminRole;
import com.yufeng.yf.admin.pojo.AdminRoleExample;
import com.yufeng.yf.common.service.base.BaseService;


/**
* @author GV
* on 2018/05/11 09:21:54
* AdminRoleService接口
*/
public interface AdminRoleService extends BaseService<AdminRole, AdminRoleExample> {

}