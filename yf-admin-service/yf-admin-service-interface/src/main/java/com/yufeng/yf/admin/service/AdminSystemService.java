package com.yufeng.yf.admin.service;

import com.yufeng.yf.admin.pojo.AdminSystem;
import com.yufeng.yf.admin.pojo.AdminSystemExample;
import com.yufeng.yf.common.service.base.BaseService;


/**
* @author GV
* on 2018/05/13 10:25:03
* AdminSystemService接口
*/
public interface AdminSystemService extends BaseService<AdminSystem, AdminSystemExample> {

}