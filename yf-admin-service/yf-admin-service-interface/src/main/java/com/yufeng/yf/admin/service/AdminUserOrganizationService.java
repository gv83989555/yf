package com.yufeng.yf.admin.service;

import com.yufeng.yf.admin.pojo.AdminUserOrganization;
import com.yufeng.yf.admin.pojo.AdminUserOrganizationExample;
import com.yufeng.yf.common.service.base.BaseService;
import com.yufeng.yf.common.web.entity.ServerResponse;


/**
* @author GV
* on 2018/05/13 10:25:03
* AdminUserOrganizationService接口
*/
public interface AdminUserOrganizationService extends BaseService<AdminUserOrganization, AdminUserOrganizationExample> {

    ServerResponse organization(int id, String[] organizationId);
}