package com.yufeng.yf.admin.service;

import com.yufeng.yf.admin.dto.PermissionChangeData;
import com.yufeng.yf.admin.pojo.AdminUserPermission;
import com.yufeng.yf.admin.pojo.AdminUserPermissionExample;
import com.yufeng.yf.common.service.base.BaseService;
import com.yufeng.yf.common.web.entity.ServerResponse;


/**
* @author GV
* on 2018/05/13 14:38:35
* AdminUserPermissionService接口
*/
public interface AdminUserPermissionService extends BaseService<AdminUserPermission, AdminUserPermissionExample> {

    ServerResponse permission(PermissionChangeData[] changeDatas, int id);
}