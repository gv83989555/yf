package com.yufeng.yf.admin.service;

import com.yufeng.yf.admin.pojo.AdminUserRole;
import com.yufeng.yf.admin.pojo.AdminUserRoleExample;
import com.yufeng.yf.common.service.base.BaseService;
import com.yufeng.yf.common.web.entity.ServerResponse;


/**
* @author GV
* on 2018/05/11 09:21:54
* AdminUserRoleService接口
*/
public interface AdminUserRoleService extends BaseService<AdminUserRole, AdminUserRoleExample> {

    ServerResponse role(String[] roleIds, int id);
}