package com.yufeng.yf.admin.service;

import com.yufeng.yf.admin.pojo.AdminUser;
import com.yufeng.yf.admin.pojo.AdminUserExample;
import com.yufeng.yf.common.service.base.BaseService;


/**
* @author GV
* on 2018/05/11 09:21:54
* AdminUserService接口
*/
public interface AdminUserService extends BaseService<AdminUser, AdminUserExample> {

}