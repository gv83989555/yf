package com.yufeng.yf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.quartz.QuartzAutoConfiguration;


@SpringBootApplication(exclude = {QuartzAutoConfiguration.class})
public class YfAdminWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(YfAdminWebApplication.class, args);
    }
}
