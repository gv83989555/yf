package com.yufeng.yf.app.aspect;

import com.yufeng.yf.common.utils.JsonUtils;
import com.yufeng.yf.common.web.aspect.BaseLogAspect;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

/**
 * AOP的资料：
 * http://www.cnblogs.com/-bumblebee-/archive/2012/03/29/2423408.html
 * http://hotstrong.iteye.com/blog/1330046
 * http://blog.csdn.net/confirmaname/article/details/9728327
 * http://blog.csdn.net/z2007130205/article/details/25713843
 *
 * @author gv
 */

@Aspect
@Component
public class AdminControllerLogAspect extends BaseLogAspect {
    /**
     * 环绕通配符方式
     * @param pjp
     * @return
     * @throws Throwable
     */
    @Override
    @Around("execution(* com.yufeng.yf.admin.controller.*Controller.*(..))")
    public Object aroundControllerMethod(final ProceedingJoinPoint pjp) throws Throwable {
        super.logBefore(pjp);
        Object returnVal = pjp.proceed();
        super.LOG.info("3.返回值：\n" + JsonUtils.obj2StringPretty(returnVal));
        super.logAfter();
        return returnVal;
    }


}
