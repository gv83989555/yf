package com.yufeng.yf.admin.controller;

import com.yufeng.yf.common.web.entity.ServerResponse;
import com.yufeng.yf.common.web.enums.BaseResponseCode;
import com.yufeng.yf.common.web.exception.ExceptionHandlerAdvice;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.UnauthorizedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * DATE:2017/5/1
 * TIME:上午11:04
 *
 * @author gv
 */

@ControllerAdvice
public class AdminExceptionAdvice {

    private static Logger LOGGER = LoggerFactory.getLogger(ExceptionHandlerAdvice.class);

    /**
     * 登录认证异常
     */
    @ExceptionHandler({AuthenticationException.class})
    @ResponseBody
    public Object authenticationException(HttpServletRequest request, HttpServletResponse response, Exception ex) {
//        ex.printStackTrace();
        LOGGER.info("登录认证异常");
        return ServerResponse.createByErrorCodeMessage(BaseResponseCode.NEED_LOGIN);
    }

    /**
     * 权限异常
     */
    @ExceptionHandler({UnauthorizedException.class, AuthorizationException.class})
    @ResponseBody
    public Object authorizationException(HttpServletRequest request, HttpServletResponse response, Exception ex) {
//        ex.printStackTrace();
        LOGGER.info("权限异常");
        return ServerResponse.createByErrorCodeMessage(BaseResponseCode.PERMISSION_NOT_FOUND);
    }

}
