package com.yufeng.yf.admin.controller;

import com.yufeng.yf.admin.pojo.AdminLog;
import com.yufeng.yf.admin.pojo.AdminLogExample;
import com.yufeng.yf.admin.service.AdminLogService;
import com.yufeng.yf.common.utils.StringUtil;
import com.yufeng.yf.common.web.entity.ServerResponse;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author GV
 * Date:2018/5/13
 * Time:上午10:29
 */
@Controller
@RequestMapping("/manage/log")
public class AdminLogController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AdminLogController.class);

    @Autowired
    private AdminLogService adminLogService;


    @RequiresPermissions(value = "admin:log:read")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public Object list(
            @RequestParam(required = false, defaultValue = "0", value = "offset") int offset,
            @RequestParam(required = false, defaultValue = "10", value = "limit") int limit,
            @RequestParam(required = false, defaultValue = "", value = "search") String search,
            @RequestParam(required = false, value = "sort") String sort,
            @RequestParam(required = false, value = "order") String order,
            @RequestParam(required = false, value = "startDate") Long startDate,
            @RequestParam(required = false, value = "endDate") Long endDate,
            @RequestParam(required = false, value = "method") String method
    ) {
        AdminLogExample upmsLogExample = new AdminLogExample();
        if (!StringUtils.isBlank(sort) && !StringUtils.isBlank(order)) {
            upmsLogExample.setOrderByClause(StringUtil.humpToLine(sort) + " " + order);
        }
        else {
            upmsLogExample.setOrderByClause(StringUtil.humpToLine("startTime") + " " + "desc");
        }
        AdminLogExample.Criteria criteria =upmsLogExample.createCriteria();
        if (startDate!=null&&endDate!=null){
            criteria.andStartTimeBetween(startDate,endDate);
        }
        if (StringUtils.isNotBlank(method)){
            criteria.andMethodEqualTo(method);
        }
        if (StringUtils.isNotBlank(search)) {
            upmsLogExample.or()
                    .andUsernameLike("%" + search + "%");
        }
        List<AdminLog> rows = adminLogService.selectByExampleForOffsetPage(upmsLogExample, offset, limit);
        long total = adminLogService.countByExample(upmsLogExample);

        Map<String, Object> result = new HashMap<>();
        result.put("rows", rows);
        result.put("total", total);
        return result;
    }

    @RequiresPermissions(value = "admin:log:delete")
    @RequestMapping(value = "/delete/{ids}", method = RequestMethod.GET)
    @ResponseBody
    public Object delete(@PathVariable("ids") String ids) {
        int count = adminLogService.deleteByPrimaryKeys(ids,Integer.class);
        return ServerResponse.createBySuccess();
    }


}
