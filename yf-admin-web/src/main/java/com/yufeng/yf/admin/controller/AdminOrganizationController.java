package com.yufeng.yf.app.controller;

import com.baidu.unbiz.fluentvalidator.ComplexResult;
import com.baidu.unbiz.fluentvalidator.FluentValidator;
import com.baidu.unbiz.fluentvalidator.ResultCollectors;
import com.yufeng.yf.admin.pojo.AdminOrganization;
import com.yufeng.yf.admin.pojo.AdminOrganizationExample;
import com.yufeng.yf.admin.service.AdminOrganizationService;
import com.yufeng.yf.common.web.entity.ServerResponse;
import com.yufeng.yf.common.web.validator.LengthValidator;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author GV
 * Date:2018/5/13
 * Time:上午10:12
 */
@Controller
@RequestMapping("/manage/organization/")
public class AdminOrganizationController {

    @Autowired
    private AdminOrganizationService organizationService;

    @RequiresPermissions(value = "admin:organization:read")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public Object list(
            @RequestParam(required = false, defaultValue = "0", value = "offset") int offset,
            @RequestParam(required = false, defaultValue = "10", value = "limit") int limit,
            @RequestParam(required = false, defaultValue = "", value = "search") String search,
            @RequestParam(required = false, value = "sort") String sort,
            @RequestParam(required = false, value = "order") String order
    ) {
        AdminOrganizationExample organizationExample = new AdminOrganizationExample();
        if (StringUtils.isNotBlank(sort) && StringUtils.isNotBlank(order)) {
            organizationExample.setOrderByClause(sort + "_" + order);
        }
        if (!"".equals(search)) {
            organizationExample.or().andNameLike("%" + search + "%");
        }
        List<AdminOrganization> list = organizationService.selectByExampleForOffsetPage(organizationExample, offset, limit);
        int count = organizationService.countByExample(organizationExample);
        Map<String, Object> result = new HashMap<>(2);
        result.put("total", count);
        result.put("rows", list);
        return result;
    }

    @RequiresPermissions(value = "admin:organization:update")
    @RequestMapping(value = "update/{id}", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse update(AdminOrganization AdminOrganization, @PathVariable(value = "id") int id) {
        //验证参数
        ComplexResult result = FluentValidator.checkAll()
                .on(AdminOrganization.getName(), new LengthValidator(1, 20, "名称"))
                .doValidate()
                .result(ResultCollectors.toComplex());
        if (!result.isSuccess()) {
            return ServerResponse.createByErrorMessage(result.getErrors().get(0).getErrorMsg());
        }
        AdminOrganization.setOrganizationId(id);
        int count=organizationService.updateByPrimaryKeySelective(AdminOrganization);
        return ServerResponse.createBySuccess(count);
    }

    @RequiresPermissions(value = "admin:organization:create")
    @RequestMapping(value = "create", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse create(AdminOrganization AdminOrganization) {
        //验证参数
        ComplexResult result = FluentValidator.checkAll()
                .on(AdminOrganization.getName(), new LengthValidator(1, 20, "名称"))
                .doValidate()
                .result(ResultCollectors.toComplex());
        if (!result.isSuccess()) {
            return ServerResponse.createByErrorMessage(result.getErrors().get(0).getErrorMsg());
        }
        AdminOrganization.setCtime(System.currentTimeMillis());
        int count=organizationService.insertSelective(AdminOrganization);
        return ServerResponse.createBySuccess(count);
    }

    @RequiresPermissions(value = "admin:organization:delete")
    @RequestMapping(value = "delete/{ids}", method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse delete(@PathVariable(value = "ids") String ids) {
        organizationService.deleteByPrimaryKeys(ids,Integer.class);
        return ServerResponse.createBySuccess();
    }


}
