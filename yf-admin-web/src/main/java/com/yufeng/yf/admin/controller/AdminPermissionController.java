package com.yufeng.yf.app.controller;

import com.baidu.unbiz.fluentvalidator.ComplexResult;
import com.baidu.unbiz.fluentvalidator.FluentValidator;
import com.baidu.unbiz.fluentvalidator.ResultCollectors;
import com.baidu.unbiz.fluentvalidator.ValidationError;
import com.yufeng.yf.admin.pojo.AdminPermission;
import com.yufeng.yf.admin.pojo.AdminPermissionExample;
import com.yufeng.yf.admin.service.AdminPermissionService;
import com.yufeng.yf.common.web.entity.ServerResponse;
import com.yufeng.yf.common.web.enums.BaseResponseCode;
import com.yufeng.yf.common.web.validator.LengthValidator;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author GV
 * Date:2018/5/11
 * Time:下午2:21
 */
@Controller
@RequestMapping("/manage/permission/")
public class AdminPermissionController {


    @Autowired
    AdminPermissionService adminPermissionService;


    @RequiresPermissions(value = "admin:permission:read")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public Object list(
            @RequestParam(required = false, defaultValue = "0", value = "offset") int offset,
            @RequestParam(required = false, defaultValue = "10", value = "limit") int limit,
            @RequestParam(required = false, defaultValue = "", value = "search") String search,
            @RequestParam(required = false, defaultValue = "0", value = "type") int type,
            @RequestParam(required = false, value = "sort") String sort,
            @RequestParam(required = false, value = "order") String order) {
        AdminPermissionExample upmsPermissionExample = new AdminPermissionExample();
        AdminPermissionExample.Criteria criteria = upmsPermissionExample.createCriteria();
        if (0 != type) {
            criteria.andTypeEqualTo((byte) type);
        }
        if (!StringUtils.isBlank(sort) && !StringUtils.isBlank(order)) {
            upmsPermissionExample.setOrderByClause(sort + " " + order);
        }
        if (StringUtils.isNotBlank(search)) {
            upmsPermissionExample.or()
                    .andNameLike("%" + search + "%");
        }
        List<AdminPermission> rows = adminPermissionService.selectByExampleForOffsetPage(upmsPermissionExample, offset, limit);
        long total = adminPermissionService.countByExample(upmsPermissionExample);
        Map<String, Object> result = new HashMap<>(2);
        result.put("rows", rows);
        result.put("total", total);
        return result;
    }

    @RequiresPermissions(value = "admin:permission:create")
    @ResponseBody
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public Object create(AdminPermission adminPermission) {
        ComplexResult result = FluentValidator.checkAll()
                .on(adminPermission.getName(), new LengthValidator(1, 20, "名称"))
                .doValidate()
                .result(ResultCollectors.toComplex());
        if (!result.isSuccess()) {
            return ServerResponse.createByErrorCodeMessage(BaseResponseCode.ILLEGAL_ARGUMENT.getCode(),
                    result.getErrors().
                            stream().map(ValidationError::getErrorMsg).collect(Collectors.toList()).
                            stream().reduce(" ", String::concat));
        }
        long time = System.currentTimeMillis();
        adminPermission.setCtime(time);
        adminPermission.setOrders(time);

        int count = adminPermissionService.insertSelective(adminPermission);
        return ServerResponse.createBySuccess(count);
    }


    @RequiresPermissions(value = "admin:permission:update")
    @RequestMapping(value = "/update/{id}", method = RequestMethod.POST)
    @ResponseBody
    public Object update(@PathVariable("id") int id, AdminPermission adminPermission) {
        ComplexResult result = FluentValidator.checkAll()
                .on(adminPermission.getName(), new LengthValidator(1, 20, "名称"))
                .doValidate()
                .result(ResultCollectors.toComplex());
        if (!result.isSuccess()) {
            return ServerResponse.createByErrorCodeMessage(BaseResponseCode.ILLEGAL_ARGUMENT.getCode(),
                    result.getErrors().
                            stream().map(ValidationError::getErrorMsg).collect(Collectors.toList()).
                            stream().reduce(" ", String::concat));
        }
        adminPermission.setPermissionId(id);
        int count = adminPermissionService.updateByPrimaryKeySelective(adminPermission);
        return ServerResponse.createBySuccess(count);
    }

    @RequiresPermissions(value = "admin:permission:delete")
    @RequestMapping(value = "/delete/{ids}", method = RequestMethod.GET)
    @ResponseBody
    public Object delete(@PathVariable("ids") String ids) {
        int count = adminPermissionService.deleteByPrimaryKeys(ids,Integer.class);
        return ServerResponse.createBySuccess(count);
    }


    @RequiresPermissions(value = "admin:role:permission")
    @RequestMapping(value = "role/{roleId}/{systemId}", method = RequestMethod.POST)
    @ResponseBody
    public Object role(@PathVariable("roleId") int roleId, @PathVariable("systemId") int systemId) {
        return ServerResponse.createBySuccess( adminPermissionService.getTreeByRoleIdAndSystemId(roleId,systemId));
    }


    @RequiresPermissions(value = "admin:user:permission")
    @RequestMapping(value = "/user/{id}/{systemId}", method = RequestMethod.POST)
    @ResponseBody
    public Object user(@PathVariable("id") int id, @PathVariable("systemId") int systemId , HttpServletRequest request) {
         return ServerResponse.createBySuccess(   adminPermissionService.getTreeByUserIdAndSystemId(id, systemId, NumberUtils.toByte(request.getParameter("type"))));
    }


}
