package com.yufeng.yf.admin.controller;

import com.baidu.unbiz.fluentvalidator.ComplexResult;
import com.baidu.unbiz.fluentvalidator.FluentValidator;
import com.baidu.unbiz.fluentvalidator.ResultCollectors;
import com.baidu.unbiz.fluentvalidator.ValidationError;
import com.yufeng.yf.admin.dto.PermissionChangeData;
import com.yufeng.yf.admin.pojo.AdminRole;
import com.yufeng.yf.admin.pojo.AdminRoleExample;
import com.yufeng.yf.admin.service.AdminRolePermissionService;
import com.yufeng.yf.admin.service.AdminRoleService;
import com.yufeng.yf.common.utils.JsonUtils;
import com.yufeng.yf.common.web.entity.ServerResponse;
import com.yufeng.yf.common.web.enums.BaseResponseCode;
import com.yufeng.yf.common.web.validator.LengthValidator;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author GV
 * Date:2018/5/11
 * Time:下午2:11
 */

@Controller
@RequestMapping("/manage/role/")
public class AdminRoleController {

    @Autowired
    private  AdminRoleService adminRoleService;

    @Autowired
    private AdminRolePermissionService adminRolePermissionService;

    @RequiresPermissions(value = "admin:role:read")
    @RequestMapping(value = "list", method = RequestMethod.GET)
    @ResponseBody
    public Object list(
            @RequestParam(required = false, defaultValue = "0", value = "offset") int offset,
            @RequestParam(required = false, defaultValue = "10", value = "limit") int limit,
            @RequestParam(required = false, defaultValue = "", value = "search") String search,
            @RequestParam(required = false, value = "sort") String sort,
            @RequestParam(required = false, value = "order") String order) {
        AdminRoleExample upmsRoleExample = new AdminRoleExample();
        if (!StringUtils.isBlank(sort) && !StringUtils.isBlank(order)) {
            upmsRoleExample.setOrderByClause(sort + " " + order);
        }
        if (StringUtils.isNotBlank(search)) {
            upmsRoleExample.or()
                    .andTitleLike("%" + search + "%");
        }
        List<AdminRole> rows = adminRoleService.selectByExampleForOffsetPage(upmsRoleExample, offset, limit);
        long total = adminRoleService.countByExample(upmsRoleExample);
        Map<String, Object> result = new HashMap<>(2);
        result.put("rows", rows);
        result.put("total", total);
        return result;
    }

    @RequiresPermissions(value = "admin:role:create")
    @ResponseBody
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ServerResponse create(AdminRole adminRole) {
        ComplexResult result = FluentValidator.checkAll()
                .on(adminRole.getName(), new LengthValidator(1, 20, "名称"))
                .on(adminRole.getTitle(), new LengthValidator(1, 20, "标题"))
                .doValidate()
                .result(ResultCollectors.toComplex());
        if (!result.isSuccess()) {
            return ServerResponse.createByErrorCodeMessage(BaseResponseCode.ILLEGAL_ARGUMENT.getCode(),
                    result.getErrors().
                            stream().map(ValidationError::getErrorMsg).collect(Collectors.toList()).
                            stream().reduce(" ", String::concat));
        }
        long time = System.currentTimeMillis();
        adminRole.setCtime(System.currentTimeMillis());
        adminRole.setOrders(time);
        int count = adminRoleService.insertSelective(adminRole);
        return ServerResponse.createBySuccess(count);
    }

    @RequiresPermissions(value = "admin:role:delete")
    @RequestMapping(value = "/delete/{ids}", method = RequestMethod.GET)
    @ResponseBody
    public Object delete(@PathVariable("ids") String ids) {
        int count = adminRoleService.deleteByPrimaryKeys(ids,Integer.class);
        return ServerResponse.createBySuccess(count);
    }

    @RequiresPermissions(value = "admin:role:update")
    @RequestMapping(value = "/update/{id}", method = RequestMethod.POST)
    @ResponseBody
    public Object update(@PathVariable("id") int id, AdminRole adminRole) {
        ComplexResult result = FluentValidator.checkAll()
                .on(adminRole.getName(), new LengthValidator(1, 20, "名称"))
                .on(adminRole.getTitle(), new LengthValidator(1, 20, "标题"))
                .doValidate()
                .result(ResultCollectors.toComplex());
        if (!result.isSuccess()) {
            return ServerResponse.createByErrorCodeMessage(BaseResponseCode.ILLEGAL_ARGUMENT.getCode(),
                    result.getErrors().
                            stream().map(ValidationError::getErrorMsg).collect(Collectors.toList()).
                            stream().reduce(" ", String::concat));
        }
        adminRole.setRoleId(id);
        int count = adminRoleService.updateByPrimaryKeySelective(adminRole);
        return ServerResponse.createBySuccess(count);
    }

    @RequiresPermissions(value = "admin:role:permission")
    @RequestMapping(value = "/permission/{id}", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse permission(@PathVariable("id") int id, String data) {
        return adminRolePermissionService.rolePermission(JsonUtils.jsonToList(data,PermissionChangeData.class).toArray(new PermissionChangeData[]{}), id);
    }


}
