package com.yufeng.yf.admin.controller;

import com.yufeng.yf.admin.shiro.session.AdminSessionDao;
import com.yufeng.yf.common.web.entity.ServerResponse;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * 会话管理controller
 * @author shuzheng
 * @date 2017/2/28
 */
@Controller
@RequestMapping("/manage/session")
public class AdminSessionController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AdminSessionController.class);

    @Autowired
    private AdminSessionDao sessionDAO;

    @RequiresPermissions(value = "admin:session:read")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public Object list(
            @RequestParam(required = false, defaultValue = "0", value = "offset") int offset,
            @RequestParam(required = false, defaultValue = "10", value = "limit") int limit) {
        return sessionDAO.getActiveSessions(offset, limit);
    }

    @RequiresPermissions(value = "admin:session:forceout")
    @RequestMapping(value = "/forceout/{ids}", method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse forceout(@PathVariable("ids") String ids) {
        //主要思路：将用户SESSION打上强制退出的标记，后续走拦截器时候判断处理
        int count = sessionDAO.forceOut(ids);
        return ServerResponse.createBySuccess();
    }

}