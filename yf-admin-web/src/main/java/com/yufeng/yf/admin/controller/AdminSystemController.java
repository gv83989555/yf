package com.yufeng.yf.admin.controller;

import com.baidu.unbiz.fluentvalidator.ComplexResult;
import com.baidu.unbiz.fluentvalidator.FluentValidator;
import com.baidu.unbiz.fluentvalidator.ResultCollectors;
import com.yufeng.yf.admin.pojo.AdminSystem;
import com.yufeng.yf.admin.pojo.AdminSystemExample;
import com.yufeng.yf.admin.service.AdminSystemService;
import com.yufeng.yf.common.web.entity.ServerResponse;
import com.yufeng.yf.common.web.validator.LengthValidator;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author GV
 * Date:2018/5/13
 * Time:上午10:28
 */
@Controller
@RequestMapping("/manage/system")
public class AdminSystemController {

    @Autowired
    private AdminSystemService adminSystemService;

    @RequiresPermissions(value = "admin:system:read")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public Object list(
            @RequestParam(required = false, defaultValue = "0", value = "offset") int offset,
            @RequestParam(required = false, defaultValue = "10", value = "limit") int limit,
            @RequestParam(required = false, defaultValue = "", value = "search") String search,
            @RequestParam(required = false, value = "sort") String sort,
            @RequestParam(required = false, value = "order") String order
    ) {
        AdminSystemExample adminSystemExample = new AdminSystemExample();
        if (StringUtils.isNotBlank(sort) && StringUtils.isNotBlank(order)) {
            adminSystemExample.setOrderByClause(sort + " " + order);
        }
        if (StringUtils.isNotBlank(search)) {
            adminSystemExample.or().andTitleLike("%" + search + "%");
        }
        List<AdminSystem> systemList = adminSystemService.selectByExampleForOffsetPage(adminSystemExample, offset, limit);
        long total = adminSystemService.countByExample(adminSystemExample);
        Map<String, Object> result = new HashMap<>(2);
        result.put("rows", systemList);
        result.put("total", total);
        return result;
    }

    @RequestMapping(value = "/selectList", method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse selectList(
            @RequestParam(required = false, defaultValue = "0", value = "offset") int offset,
            @RequestParam(required = false, defaultValue = "10", value = "limit") int limit,
            @RequestParam(required = false, defaultValue = "", value = "search") String search,
            @RequestParam(required = false, value = "sort") String sort,
            @RequestParam(required = false, value = "order") String order
    ) {
        AdminSystemExample upmsSystemExample = new AdminSystemExample();
        if (StringUtils.isNotBlank(sort) && StringUtils.isNotBlank(order)) {
            upmsSystemExample.setOrderByClause(sort + " " + order);
        }
        if (StringUtils.isNotBlank(search)) {
            upmsSystemExample.or().andTitleLike("%" + search + "%");
        }
        List<AdminSystem> systemList = adminSystemService.selectByExampleForOffsetPage(upmsSystemExample, offset, limit);
        return ServerResponse.createBySuccess(systemList);
    }

    @RequiresPermissions(value = "admin:system:create")
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse create(AdminSystem adminSystem) {
        //验证参数
        ComplexResult result = FluentValidator.checkAll()
                .on(adminSystem.getTitle(), new LengthValidator(1, 20, "标题"))
                .on(adminSystem.getName(), new LengthValidator(1, 20, "名称"))
                .doValidate()
                .result(ResultCollectors.toComplex());
        if (!result.isSuccess()) {
            return ServerResponse.createByErrorMessage(result.getErrors().get(0).getErrorMsg());
        }
        long time = System.currentTimeMillis();
        adminSystem.setCtime(System.currentTimeMillis());
        adminSystem.setOrders(time);
        int count = adminSystemService.insertSelective(adminSystem);
        return ServerResponse.createBySuccess(count);
    }

    @RequiresPermissions(value = "admin:system:update")
    @RequestMapping(value = "/update/{id}", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse update(AdminSystem adminSystem, @PathVariable(value = "id") int id) {
        //验证参数
        ComplexResult result = FluentValidator.checkAll()
                .on(adminSystem.getTitle(), new LengthValidator(1, 20, "标题"))
                .on(adminSystem.getName(), new LengthValidator(1, 20, "名称"))
                .doValidate()
                .result(ResultCollectors.toComplex());
        if (!result.isSuccess()) {
            return ServerResponse.createByErrorMessage(result.getErrors().get(0).getErrorMsg());
        }
        adminSystem.setSystemId(id);
        int count = adminSystemService.updateByPrimaryKeySelective(adminSystem);
        return ServerResponse.createBySuccess(count);
    }

    @RequiresPermissions(value = "admin:system:delete")
    @RequestMapping(value = "/delete/{ids}", method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse delete(@PathVariable(value = "ids") String ids) {
        int count = adminSystemService.deleteByPrimaryKeys(ids,Integer.class);
        return ServerResponse.createBySuccess(count);
    }
}
