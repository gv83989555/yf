package com.yufeng.yf.admin.controller;


import com.baidu.unbiz.fluentvalidator.ComplexResult;
import com.baidu.unbiz.fluentvalidator.FluentValidator;
import com.baidu.unbiz.fluentvalidator.ResultCollectors;
import com.baidu.unbiz.fluentvalidator.ValidationError;
import com.yufeng.yf.admin.dto.PermissionChangeData;
import com.yufeng.yf.admin.pojo.*;
import com.yufeng.yf.admin.service.*;
import com.yufeng.yf.common.utils.JsonUtils;
import com.yufeng.yf.common.utils.MD5Util;
import com.yufeng.yf.common.web.entity.ServerResponse;
import com.yufeng.yf.common.web.enums.BaseResponseCode;
import com.yufeng.yf.common.web.validator.LengthValidator;
import com.yufeng.yf.common.web.validator.ObjectNotNullValidator;
import com.yufeng.yf.common.web.validator.StringNotNullValidator;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;


/**
 * @author gv
 */
@Controller
@RequestMapping("manage/user/")
public class AdminUserController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AdminUserController.class);
    @Autowired
    private AdminUserService adminUserService;

    @Autowired
    private AdminRoleService adminRoleService;

    @Autowired
    private AdminUserRoleService adminUserRoleService;

    @Autowired
    private AdminOrganizationService adminOrganizationService;

    @Autowired
    private AdminUserOrganizationService adminUserOrganizationService;

    @Autowired
    private AdminPermissionService adminPermissionService;

    @Autowired
    private AdminUserPermissionService adminUserPermissionService;


    @RequestMapping(value = "list", method = RequestMethod.GET)
    @ResponseBody
    public Object list(@RequestParam(required = false, defaultValue = "0", value = "offset") int offset,
                       @RequestParam(required = false, defaultValue = "10", value = "limit") int limit,
                       @RequestParam(required = false, defaultValue = "", value = "search") String search,
                       @RequestParam(required = false, value = "sort") String sort,
                       @RequestParam(required = false, value = "order") String order) {
        AdminUserExample upmsUserExample = new AdminUserExample();
        if (!StringUtils.isBlank(sort) && !StringUtils.isBlank(order)) {
            upmsUserExample.setOrderByClause(sort + " " + order);
        }
        if (StringUtils.isNotBlank(search)) {
            upmsUserExample.or()
                    .andRealnameLike("%" + search + "%");
            upmsUserExample.or()
                    .andUsernameLike("%" + search + "%");
        }
        List<AdminUser> rows = adminUserService.selectByExampleForOffsetPage(upmsUserExample, offset, limit);
        long total = adminUserService.countByExample(upmsUserExample);
        Map<String, Object> result = new HashMap<>();
        result.put("rows", rows);
        result.put("total", total);
        return result;
    }



    @RequiresPermissions(value = "admin:user:create")
    @RequestMapping(value = "create", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse create(AdminUser adminUser) {

        ComplexResult result = FluentValidator.checkAll()
                .on(adminUser.getUsername(), new LengthValidator(1, 20, "帐号"))
                .on(adminUser.getPassword(), new LengthValidator(5, 32, "密码"))
                .on(adminUser.getRealname(), new ObjectNotNullValidator("姓名"))
                .doValidate()
                .result(ResultCollectors.toComplex());
        if (!result.isSuccess()) {
            return ServerResponse.createByErrorCodeMessage(BaseResponseCode.ILLEGAL_ARGUMENT.getCode(),
                    result.getErrors().
                            stream().map(ValidationError::getErrorMsg).collect(Collectors.toList()).
                            stream().reduce("", String::concat));
        }

        AdminUserExample upmsUserExample = new AdminUserExample();
        upmsUserExample.createCriteria()
                .andUsernameEqualTo(adminUser.getUsername());
        int count = adminUserService.countByExample(upmsUserExample);
        if (count > 0) {
            return ServerResponse.createByErrorCodeMessage(BaseResponseCode.USER_EXISTED);
        }

        adminUser.setSalt(UUID.randomUUID().toString().replace("-", ""));
        adminUser.setPassword(MD5Util.md5(adminUser.getPassword() + adminUser.getSalt()));
        adminUser.setCtime(System.currentTimeMillis());
        adminUser.setLastLoginTime(System.currentTimeMillis());
        count = adminUserService.insert(adminUser);
        if (count == 0) {
            return ServerResponse.createByError();
        }
        LOGGER.info("新增用户，主键：userId={}", count);
        return ServerResponse.createBySuccess();
    }
    @RequiresPermissions(value = "admin:user:update")
    @RequestMapping(value = "/update/{id}", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse update(@PathVariable("id") int id, AdminUser adminUser) {

        if (adminUser.getPassword() != null) {
            adminUser.setSalt(UUID.randomUUID().toString().replace("-", ""));
            adminUser.setPassword(MD5Util.md5(adminUser.getPassword() + adminUser.getSalt()));
        }
        // 不允许直接改账号
        adminUser.setUsername(null);
        adminUser.setUserId(id);
        int count = adminUserService.updateByPrimaryKeySelective(adminUser);
        if (count == 0) {
            return ServerResponse.createByError();
        }
        return ServerResponse.createBySuccess(count);
    }

    //    @RequiresPermissions(value = "admin:user:updatePwd")
    @RequestMapping(value = "/updatePwd", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse update(String oldPwd, String newPwd) {
        ComplexResult result = FluentValidator.checkAll()
                .on(oldPwd, new StringNotNullValidator( "旧密码"))
                .on(newPwd, new StringNotNullValidator("新密码"))
                .doValidate()
                .result(ResultCollectors.toComplex());
        if (!result.isSuccess()) {
            return ServerResponse.createByErrorCodeMessage(BaseResponseCode.ILLEGAL_ARGUMENT.getCode(),
                    result.getErrors().
                            stream().map(ValidationError::getErrorMsg).collect(Collectors.toList()).
                            stream().reduce("", String::concat));
        }
        Session session = SecurityUtils.getSubject().getSession();
        Integer adminUserId = (Integer) session.getAttribute("adminUserId");
        AdminUser adminUser = adminUserService.selectByPrimaryKey(adminUserId);
        if (adminUser == null) {
            LOGGER.error("找不到当前用户");
            return ServerResponse.createByError();
        }
        if (!adminUser.getPassword().equals(MD5Util.md5(oldPwd + adminUser.getSalt()))) {
            return ServerResponse.createByErrorCodeMessage(BaseResponseCode.PWD_ERROR.getCode(), BaseResponseCode.PWD_ERROR.getDesc());
        }
        adminUser.setSalt(UUID.randomUUID().toString().replace("-", ""));
        adminUser.setPassword(MD5Util.md5(newPwd + adminUser.getSalt()));

        int count = adminUserService.updateByPrimaryKeySelective(adminUser);
        if (count == 0) {
            return ServerResponse.createByError();
        }
        return ServerResponse.createBySuccess();
    }

    @RequiresPermissions(value = "admin:user:delete")
    @RequestMapping(value = "/delete/{ids}", method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse delete(@PathVariable("ids") String ids) {
        int count = adminUserService.deleteByPrimaryKeys(ids, Integer.class);
        if (count == 0) {
            return ServerResponse.createByError();
        }
        return ServerResponse.createBySuccess(count);
    }

    @RequestMapping(value = "getUserInfo", method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse getUserInfo() {
        Subject subject = SecurityUtils.getSubject();
        String username = (String) subject.getPrincipal();
        String realname = (String) subject.getSession().getAttribute("realname");

        return ServerResponse.createBySuccess(realname);
    }

    @RequiresPermissions(value = "admin:user:role")
    @RequestMapping(value = "/role/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse role(@PathVariable("id") int id) {
        // 所有角色
        List<AdminRole> upmsRoles = adminRoleService.selectByExample(new AdminRoleExample());
        // 用户拥有角色
        AdminUserRoleExample upmsUserRoleExample = new AdminUserRoleExample();
        upmsUserRoleExample.createCriteria()
                .andUserIdEqualTo(id);
        List<AdminUserRole> upmsUserRoles = adminUserRoleService.selectByExample(upmsUserRoleExample);
        Map<String, Object> map = new HashMap<>();
        map.put("upmsRoles", upmsRoles);
        map.put("upmsUserRoles", upmsUserRoles);
        return ServerResponse.createBySuccess(map);
    }


    @RequiresPermissions(value = "admin:user:role")
    @RequestMapping(value = "/role/{id}", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse userRole(@PathVariable("id") int id, String[] roleIds) {
        return adminUserRoleService.role(roleIds, id);
    }

    @RequiresPermissions(value = "admin:user:organization")
    @RequestMapping(value = "/organization/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse organization(@PathVariable(value = "id") int id) {
        //查找所有部门
        List<AdminOrganization> organizationList = adminOrganizationService.selectByExample(new AdminOrganizationExample());
        //查找用户关联部门
        AdminUserOrganizationExample upmsUserOrganizationExample = new AdminUserOrganizationExample();
        upmsUserOrganizationExample.createCriteria().andUserIdEqualTo(id);
        List<AdminUserOrganization> upmsUserOrganizationList = adminUserOrganizationService.selectByExample(upmsUserOrganizationExample);

        Map<String, Object> result = new HashMap<>(2);
        result.put("organizationList", organizationList);
        result.put("upmsUserOrganizationList", upmsUserOrganizationList);
        return ServerResponse.createBySuccess(result);
    }

    @RequiresPermissions(value = "admin:user:organization")
    @RequestMapping(value = "/organization/{id}", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse userOrganization(@PathVariable(value = "id") int id, String[] organizationId) {
        return adminUserOrganizationService.organization(id, organizationId);
    }



    @RequiresPermissions(value = "admin:user:permission")
    @RequestMapping(value = "/permission/{id}", method = RequestMethod.POST)
    @ResponseBody
    public Object permission(@PathVariable("id") int id,
//                             @RequestBody PermissoinChangeData[] changeDatas
                             String data
    ) {
        return adminUserPermissionService.permission(JsonUtils.jsonToList(data, PermissionChangeData.class).toArray(new PermissionChangeData[]{}), id);
    }


}
