package com.yufeng.yf.admin.controller;

import com.baidu.unbiz.fluentvalidator.ComplexResult;
import com.baidu.unbiz.fluentvalidator.FluentValidator;
import com.baidu.unbiz.fluentvalidator.ResultCollectors;
import com.baidu.unbiz.fluentvalidator.ValidationError;
import com.yufeng.yf.admin.constant.AdminRedisKeyConst;
import com.yufeng.yf.admin.shiro.session.AdminSession;
import com.yufeng.yf.admin.shiro.session.AdminSessionDao;
import com.yufeng.yf.admin.shiro.session.SerializableUtil;
import com.yufeng.yf.common.redis.RedisClient;
import com.yufeng.yf.common.web.entity.ServerResponse;
import com.yufeng.yf.common.web.enums.BaseResponseCode;
import com.yufeng.yf.common.web.validator.StringNotNullValidator;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.stream.Collectors;

/**
 * @author GV
 * Date:2018/5/11
 * Time:上午9:23
 */
@Controller
@RequestMapping("security/")
public class SecurityController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SecurityController.class);


    @Autowired
    private RedisClient redisClient;

    @Autowired
    private AdminSessionDao sessionDao;

    @RequestMapping(value = "login", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse login(String username,
                                String password,
                                @RequestParam(value = "backUrl", defaultValue = "") String backUrl) {
        ComplexResult result = FluentValidator.checkAll()
                .on(username, new StringNotNullValidator("用户名"))
                .on(password, new StringNotNullValidator("密码"))
                .doValidate()
                .result(ResultCollectors.toComplex());
        if (!result.isSuccess()) {
            return ServerResponse.createByErrorCodeMessage(BaseResponseCode.ILLEGAL_ARGUMENT.getCode(),
                    result.getErrors().
                            stream().map(ValidationError::getErrorMsg).collect(Collectors.toList()).
                            stream().reduce("", String::concat));
        }
        Subject subject = SecurityUtils.getSubject();
        Session session = subject.getSession();
        String sessionId = session.getId().toString();
        if (!subject.isAuthenticated()) {
            // 使用shiro认证
            UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(username, password);
            try {
                subject.login(usernamePasswordToken);
            } catch (UnknownAccountException e) {
                return ServerResponse.createByErrorCodeMessage(BaseResponseCode.NONE_USER.getCode(), BaseResponseCode.NONE_USER.getDesc());
            } catch (IncorrectCredentialsException e) {
                return ServerResponse.createByErrorCodeMessage(BaseResponseCode.PWD_ERROR.getCode(), BaseResponseCode.PWD_ERROR.getDesc());
            } catch (LockedAccountException e) {
                return ServerResponse.createByErrorCodeMessage(BaseResponseCode.LOCKED_USER.getCode(), BaseResponseCode.LOCKED_USER.getDesc());
            }
            //读取当前会话
            AdminSession adminSession = (AdminSession) sessionDao.doReadSession(session.getId());
            if (null != adminSession) {
                adminSession.setStatus(AdminSession.OnlineStatus.on_line);
                // 1更新session状态
                redisClient.setex(AdminRedisKeyConst.ADMIN_SHIRO_SESSION_ID + sessionId, (int) session.getTimeout() / 1000, SerializableUtil.serialize(adminSession));
                // 2全局会话sessionId列表，供会话管理
                redisClient.lpush(AdminRedisKeyConst.ADMIN_SERVER_SESSION_IDS, sessionId);
            }
        }
        HashMap<String, Object> map = new HashMap<>(1);
        map.put("backUrl", backUrl);
        return ServerResponse.createBySuccess(map);
    }

    @RequestMapping(value = "logout", method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse logout() {
        // shiro退出登录
        SecurityUtils.getSubject().logout();
        return ServerResponse.createBySuccess();
    }

}
