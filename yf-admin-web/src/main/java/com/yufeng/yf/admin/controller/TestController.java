package com.yufeng.yf.admin.controller;


import com.yufeng.yf.common.web.entity.ServerResponse;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
public class TestController {


    @GetMapping("/manage/test")
    public Object test() {
        HashMap<String, Object> result = new HashMap<>(2);
        result.put("hello", "hello world");
        return ServerResponse.createBySuccess(result);
    }






}
