package com.yufeng.yf.admin.shiro.config;

import com.yufeng.yf.admin.shiro.filter.AdminAuthenticationFilter;
import com.yufeng.yf.admin.shiro.filter.AdminSessionForceLogoutFilter;
import com.yufeng.yf.admin.shiro.listener.AdminSessionListener;
import com.yufeng.yf.admin.shiro.realm.CustomShiroRealm;
import com.yufeng.yf.admin.shiro.session.AdminSessionDao;
import com.yufeng.yf.admin.shiro.session.AdminSessionFactory;
import org.apache.shiro.cache.MemoryConstrainedCacheManager;
import org.apache.shiro.session.SessionListener;
import org.apache.shiro.spring.LifecycleBeanPostProcessor;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.servlet.SimpleCookie;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.apache.shiro.mgt.SecurityManager;
import org.springframework.context.annotation.DependsOn;
import org.springframework.web.filter.DelegatingFilterProxy;

import javax.servlet.Filter;
import javax.servlet.FilterConfig;

import java.util.*;

/**
 * @author 高巍
 * @createTime 2018年09月03日 13:53
 * @description shiro配置
 */
@Configuration
public class ShiroConfiguration {

    @Bean
    public ShiroFilterFactoryBean shiroFilter(SecurityManager securityManager){
        ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
        // 必须设置 SecurityManager
        shiroFilterFactoryBean.setSecurityManager(securityManager);
        // setLoginUrl
        shiroFilterFactoryBean.setLoginUrl("/manage/user/getUserInfo");
        // 重写认证拦截器
        HashMap<String, Filter> filters = new HashMap<>(2);
        filters.put("authc",adminAuthenticationFilter());
        filters.put("adminSessionForceLogoutFilter",adminSessionForceLogoutFilter());
        shiroFilterFactoryBean.setFilters(filters);
        // 设置拦截器
        Map<String, String> filterChainDefinitionMap = new LinkedHashMap<>();
        filterChainDefinitionMap.put("/manage/**", "authc,adminSessionForceLogoutFilter");
        filterChainDefinitionMap.put("/**", "anon");

        shiroFilterFactoryBean.setFilterChainDefinitionMap(filterChainDefinitionMap);
        return shiroFilterFactoryBean;
    }


    /**
     * 注入 securityManager
     */
    @Bean
    public SecurityManager securityManager() {
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        // 设置realm.
        securityManager.setRealm(customShiroRealm());
        securityManager.setSessionManager(sessionManager());
        securityManager.setCacheManager(cacheManager());
        return securityManager;
    }

    /**
     * 自定义realm
     * @return
     */
    @Bean
    public CustomShiroRealm customShiroRealm(){
        return new CustomShiroRealm();
    }

    /**
     * 重写认证拦截器
     * @return
     */
    @Bean
    public AdminAuthenticationFilter adminAuthenticationFilter(){
        return new AdminAuthenticationFilter();
    }

    /**
     * 强制登出拦截器
     * @return
     */
    @Bean
    public AdminSessionForceLogoutFilter adminSessionForceLogoutFilter(){
        return new AdminSessionForceLogoutFilter();
    }

    /**
     * 会话DAO，可重写，持久化session
     * @return
     */
    @Bean
    public AdminSessionDao sessionDao(){
        return new AdminSessionDao();
    }

    /**
     * 会话监听器
     * @return
     */
    @Bean
    public AdminSessionListener sessionListener(){
        return new AdminSessionListener();
    }

    /**
     * session工厂
     * @return
     */
    @Bean
    public AdminSessionFactory adminSessionFactory(){
        return new AdminSessionFactory();
    }

    /**
     * 会话Cookie模板
     * @return
     */
    @Bean
    public SimpleCookie simpleCookie(){
        SimpleCookie  simpleCookie=new SimpleCookie();
        //设置Cookie名字, 默认为: JSESSIONID 问题: 与SERVLET容器名冲突, 如JETTY, TOMCAT 等默认JSESSIONID,
        //当跳出SHIRO SERVLET时如ERROR-PAGE容器会为JSESSIONID重新分配值导致登录会话丢失!
        simpleCookie.setName("SHIRO_SESSION_ID");
        simpleCookie.setHttpOnly(true);
        simpleCookie.setMaxAge(-1);
        simpleCookie.setPath("/");
        return simpleCookie;
    }

    /**
     * session工厂
     * @return
     */
    @Bean
    public AdminSessionFactory sessionFactory(){
        return new AdminSessionFactory();
    }

    /**
     * 回话管理器
     * @return
     */
    @Bean
    public DefaultWebSessionManager  sessionManager(){
        DefaultWebSessionManager defaultWebSessionManager=new DefaultWebSessionManager();
        defaultWebSessionManager.setSessionIdCookie(simpleCookie());
        //SESSION过期时间 毫秒
        defaultWebSessionManager.setGlobalSessionTimeout(86400000);
        defaultWebSessionManager.setDeleteInvalidSessions(true);
        defaultWebSessionManager.setSessionValidationSchedulerEnabled(false);
        defaultWebSessionManager.setSessionValidationInterval(86400000);
        defaultWebSessionManager.setSessionDAO(sessionDao());
        List<SessionListener> sessionListeners = new ArrayList<>(1);
        sessionListeners.add(sessionListener());
        defaultWebSessionManager.setSessionListeners(sessionListeners);
        defaultWebSessionManager.setSessionFactory(sessionFactory());
        return defaultWebSessionManager;
    }

    /**
     * 缓存管理器
     * @return
     */
    @Bean
    public MemoryConstrainedCacheManager cacheManager(){
        return new MemoryConstrainedCacheManager();
    }

    /**
     * Shiro生命周期处理器
     * @return
     */
    @Bean
    public LifecycleBeanPostProcessor lifecycleBeanPostProcessor(){
        return new  LifecycleBeanPostProcessor();
    }

    /**
     * 开启Shiro Spring AOP权限注解@RequiresPermissions的支持
     * @return
     */
    @Bean
    @DependsOn("lifecycleBeanPostProcessor")
    public DefaultAdvisorAutoProxyCreator defaultAdvisorAutoProxyCreator(){
        DefaultAdvisorAutoProxyCreator defaultAdvisorAutoProxyCreator = new DefaultAdvisorAutoProxyCreator();
        defaultAdvisorAutoProxyCreator.setProxyTargetClass(true);
        return defaultAdvisorAutoProxyCreator;
    }

    @Bean
    public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(SecurityManager securityManager){
        AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor = new AuthorizationAttributeSourceAdvisor();
        authorizationAttributeSourceAdvisor.setSecurityManager(securityManager);
        return authorizationAttributeSourceAdvisor;
    }
}
