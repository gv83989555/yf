package com.yufeng.yf.admin.shiro.filter;


import com.yufeng.yf.common.utils.JsonUtils;
import com.yufeng.yf.common.web.entity.ServerResponse;
import com.yufeng.yf.common.web.enums.BaseResponseCode;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authc.AuthenticationFilter;
import org.apache.shiro.web.util.WebUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;


/**
 * 重写认证过滤器
 * @author gv
 */
public class AdminAuthenticationFilter extends AuthenticationFilter {

    private static final Logger LOGGER = LoggerFactory.getLogger(AdminAuthenticationFilter.class);


    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {
        Subject subject = getSubject(request, response);
        return subject.isAuthenticated();
    }

    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
        // server需要登录
        String responseJson = JsonUtils.objectToJson(
                ServerResponse.createByErrorCodeMessage(
                        BaseResponseCode.NEED_LOGIN.getCode(),
                        BaseResponseCode.NEED_LOGIN.getDesc()));

        response.setContentType("application/json;charset=UTF-8");
        WebUtils.
                toHttp(response).
                getWriter().
                print(responseJson);
//        LOGGER.info("认证失败，返回信息：{}", responseJson);
        return false;
    }


}
