package com.yufeng.yf.admin.shiro.filter;

import com.yufeng.yf.common.utils.JsonUtils;
import com.yufeng.yf.common.web.entity.ServerResponse;
import com.yufeng.yf.common.web.enums.BaseResponseCode;
import org.apache.shiro.session.Session;
import org.apache.shiro.web.filter.AccessControlFilter;
import org.apache.shiro.web.util.WebUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * 自定义强制退出过滤器
 * 用于会话管理强制登出用户，当用户再次访问时候通过此过滤器处理
 *
 * @author gv
 */
public class AdminSessionForceLogoutFilter extends AccessControlFilter {

    private final static Logger LOGGER = LoggerFactory.getLogger(AdminSessionForceLogoutFilter.class);

    /**
     * 是否允许继续访问，mappedValue 就是[urls]配置中拦截器参数部分，
     * 允许继续访问，返回 true，
     * 拒绝继续访问，返回 false;
     */
    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) throws Exception {
        Session session = super.getSubject(request, response).getSession(false);

        //未登陆，放行
        return session == null || session.getAttribute("FORCE_LOGOUT") == null;
    }

    /**
     * 拒绝继续访问时的处理方式，
     * 自己不处理了继续执行截器链，返回 true，
     * 自己已经处理了(比如重定向到另一个页面)，返回 false ；
     */
    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
        super.getSubject(request, response).logout();
        LOGGER.info("onAccessDenied");

        // server需要登录
        String responseJson = JsonUtils.objectToJson(
                ServerResponse.createByErrorCodeMessage(
                        BaseResponseCode.NEED_LOGIN.getCode(),
                        BaseResponseCode.NEED_LOGIN.getDesc()));

        response.setContentType("application/json;charset=UTF-8");
        WebUtils.
                toHttp(response).
                getWriter().
                print(responseJson);

        return false;
    }

}
