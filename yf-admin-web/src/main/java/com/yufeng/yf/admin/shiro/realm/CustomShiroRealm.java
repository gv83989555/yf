package com.yufeng.yf.admin.shiro.realm;

import com.yufeng.yf.admin.constant.AdminUserConstant;
import com.yufeng.yf.admin.pojo.AdminPermission;
import com.yufeng.yf.admin.pojo.AdminRole;
import com.yufeng.yf.admin.pojo.AdminUser;
import com.yufeng.yf.common.utils.MD5Util;
import com.yufeng.yf.admin.service.AdminApiService;
import com.yufeng.yf.admin.service.AdminUserService;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 用户认证和授权
 * @author gv
 */
public class CustomShiroRealm extends AuthorizingRealm {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomShiroRealm.class);

    @Autowired
    private AdminApiService adminApiService;
    @Autowired
    private AdminUserService adminUserService;
    /**
     * 认证：登录时调用
     * @param authenticationToken
     * @return
     * @throws AuthenticationException
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {

        String username = (String) authenticationToken.getPrincipal();
        String password = new String((char[]) authenticationToken.getCredentials());

        LOGGER.info("--------------------------- 认证---------------------------- username:{}",username);

        // 查询用户信息
        AdminUser adminUser = adminApiService.selectAdminUserByUsername(username);

        LOGGER.info("{}",adminUser);
        if (null == adminUser) {
            throw new UnknownAccountException();
        }
        if (!adminUser.getPassword().equals(MD5Util.md5(password + adminUser.getSalt()))) {
            throw new IncorrectCredentialsException();
        }

        if (adminUser.getLocked() == AdminUserConstant.STATE_LOCKED) {
            throw new LockedAccountException();
        }

        adminUser.setLastLoginTime(System.currentTimeMillis());
        adminUserService.updateByPrimaryKeySelective(adminUser);

        Subject subject= SecurityUtils.getSubject();
        Session session=subject.getSession();
        session.setAttribute("username",username);
        session.setAttribute("adminUserId",adminUser.getUserId());
        session.setAttribute("realname",adminUser.getRealname());
        return new SimpleAuthenticationInfo(username, password, getName());
    }

    /**
     * 授权：验证权限时调用
     * @param principalCollection
     * @return
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        String username = (String) principalCollection.getPrimaryPrincipal();

        // 查询用户信息
        AdminUser adminUser = adminApiService.selectAdminUserByUsername(username);
        LOGGER.info("--------------------------- 授权---------------------------- username:{}",username);

        // 当前用户所有角色
        List<AdminRole> adminRoles = adminApiService.selectAdminRoleByAdminUserId(adminUser.getUserId());

        Set<String> roles = new HashSet<>();
        for (AdminRole adminRole : adminRoles) {
            if (StringUtils.isNotBlank(adminRole.getName())) {
                roles.add(adminRole.getName());
            }
        }

        // 当前用户所有权限
        List<AdminPermission> adminPermissions = adminApiService.selectAdminPermissionByAdminUserId(adminUser.getUserId());

        Set<String> permissions = new HashSet<>();
        for (AdminPermission adminPermission : adminPermissions) {
            if (StringUtils.isNotBlank(adminPermission.getPermissionValue())) {
                permissions.add(adminPermission.getPermissionValue());
            }
        }

        SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
        simpleAuthorizationInfo.setStringPermissions(permissions);
        simpleAuthorizationInfo.setRoles(roles);
        return simpleAuthorizationInfo;
    }

}
