package com.yufeng.yf.admin.shiro.session;

import com.yufeng.yf.admin.constant.AdminRedisKeyConst;
import com.yufeng.yf.common.redis.RedisClient;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.ValidatingSession;
import org.apache.shiro.session.mgt.eis.CachingSessionDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 基于redis的sessionDao，缓存共享session
 *
 * @author gv
 */
public class AdminSessionDao extends CachingSessionDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(AdminSessionDao.class);

    @Autowired
    private RedisClient redisClient;

    @Override
    protected Serializable doCreate(Session session) {
        Serializable sessionId = generateSessionId(session);
        LOGGER.info("doCreate sessionId:{}", sessionId);
        super.assignSessionId(session, sessionId);
        //缓存当前session
        String serialize = SerializableUtil.serialize(session);
        redisClient.setex(AdminRedisKeyConst.ADMIN_SHIRO_SESSION_ID + sessionId, (int) session.getTimeout() / 1000, serialize);
        return sessionId;
    }

    @Override
    public Session doReadSession(Serializable sessionId) {
        LOGGER.info("doReadSession sessionId:{}", sessionId);
        return SerializableUtil.deserialize(redisClient.get(AdminRedisKeyConst.ADMIN_SHIRO_SESSION_ID + sessionId));
    }

    @Override
    protected void doUpdate(Session session) {
        // 如果会话过期/停止 没必要再更新了
        if (session instanceof ValidatingSession && !((ValidatingSession) session).isValid()) {
            LOGGER.info("更新SESSION判断已过期---return");
            return;
        }
        // 更新session的最后一次访问时间
        AdminSession adminSession = (AdminSession) session;
        AdminSession cacheAdminSession = (AdminSession) doReadSession(session.getId());
        if (null != cacheAdminSession) {
            adminSession.setStatus(cacheAdminSession.getStatus());
            adminSession.setAttribute(AdminRedisKeyConst.FORCE_LOGOUT, cacheAdminSession.getAttribute(AdminRedisKeyConst.FORCE_LOGOUT));
        }
        redisClient.setex(AdminRedisKeyConst.ADMIN_SHIRO_SESSION_ID + session.getId(), (int) session.getTimeout() / 1000, SerializableUtil.serialize(adminSession));
        LOGGER.info("续约Redis session成功 SESSIONID={}", session.getId());
    }

    @Override
    protected void doDelete(Session session) {
        String sessionId = session.getId().toString();
        LOGGER.info("doDelete sessionId:{}", sessionId);
        // 删除session
        redisClient.del(AdminRedisKeyConst.ADMIN_SHIRO_SESSION_ID + sessionId);
    }

    /**
     * 获取会话列表
     *
     * @param offset
     * @param limit
     * @return
     */
    public Map getActiveSessions(int offset, int limit) {
        Map sessions = new HashMap();
        // 获取当前页会话详情
        List<String> ids = redisClient.lrange(AdminRedisKeyConst.ADMIN_SERVER_SESSION_IDS, offset, (offset + limit - 1));
        HashMap<Integer, Session> sessionHashMap = new HashMap<>();
        for (String id : ids) {
            Session session = SerializableUtil.deserialize(redisClient.get(AdminRedisKeyConst.ADMIN_SHIRO_SESSION_ID + id));
            // 过滤redis过期session
            if (null == session) {
                redisClient.lrem(AdminRedisKeyConst.ADMIN_SERVER_SESSION_IDS, 1, id);
                continue;
            }
            //处理重复session
            Integer adminUserId = (Integer) session.getAttribute("adminUserId");
            if (sessionHashMap.containsKey(adminUserId)) {
                Session cacheSession = sessionHashMap.get(adminUserId);
                if (session.getLastAccessTime().getTime() > cacheSession.getLastAccessTime().getTime()) {
                    //删除旧的
                    Session put = sessionHashMap.put(adminUserId, session);
                    redisClient.lrem(AdminRedisKeyConst.ADMIN_SERVER_SESSION_IDS, 1, put.getId());
                    redisClient.del(AdminRedisKeyConst.ADMIN_SHIRO_SESSION_ID+put.getId());
                }else {
                    //删除当前
                    redisClient.lrem(AdminRedisKeyConst.ADMIN_SERVER_SESSION_IDS, 1, id);
                    redisClient.del(AdminRedisKeyConst.ADMIN_SHIRO_SESSION_ID+id);
                }
            } else {
                sessionHashMap.put(adminUserId, session);
            }
        }

        sessions.put("total", redisClient.llen(AdminRedisKeyConst.ADMIN_SERVER_SESSION_IDS));
        sessions.put("rows", sessionHashMap.values());
        return sessions;
    }

    /**
     * 强制退出
     *
     * @param ids
     * @return
     */
    public int forceOut(String ids) {
        LOGGER.info("forceOut ids:{}", ids);
        String[] sessionIds = ids.split(",");
        for (String sessionId : sessionIds) {
            // 会话增加强制退出属性标识，当此会话访问系统时，判断有该标识，则退出登录
            String session = redisClient.get(AdminRedisKeyConst.ADMIN_SHIRO_SESSION_ID + sessionId);
            AdminSession adminSession = (AdminSession) SerializableUtil.deserialize(session);
            adminSession.setStatus(AdminSession.OnlineStatus.force_logout);
            adminSession.setAttribute(AdminRedisKeyConst.FORCE_LOGOUT, AdminRedisKeyConst.FORCE_LOGOUT);

            redisClient.setex(AdminRedisKeyConst.ADMIN_SHIRO_SESSION_ID + sessionId, (int) adminSession.getTimeout() / 1000, SerializableUtil.serialize(adminSession));
        }
        return sessionIds.length;
    }

    /**
     * 更改UpmsSession在线状态
     *
     * @param sessionId
     * @param onlineStatus
     */
    public void updateStatus(Serializable sessionId, AdminSession.OnlineStatus onlineStatus) {
        AdminSession session = (AdminSession) doReadSession(sessionId);
        if (null == session) {
            return;
        }
        session.setStatus(onlineStatus);
        redisClient.setex(AdminRedisKeyConst.ADMIN_SHIRO_SESSION_ID + session.getId(), (int) session.getTimeout() / 1000, session);
    }

}
