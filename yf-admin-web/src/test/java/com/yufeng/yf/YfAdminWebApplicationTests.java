package com.yufeng.yf;

import com.yufeng.yf.admin.mapper.AdminUserMapper;
import com.yufeng.yf.admin.pojo.AdminUserExample;
import com.yufeng.yf.common.redis.RedisClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;


@RunWith(SpringRunner.class)
@SpringBootTest
public class YfAdminWebApplicationTests {


    @Autowired
    RedisClient redisClient;

    @Autowired
    AdminUserMapper adminUserMapper;


    @Test
    public void customMapper() {
        System.out.println(adminUserMapper.selectByExample(new AdminUserExample()));
    }


    @Test
    public void redis() {
        HashMap map = (HashMap) redisClient.get("APP:PARAM");
        System.out.println(map);
    }
}
