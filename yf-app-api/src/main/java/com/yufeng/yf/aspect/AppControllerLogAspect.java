package com.yufeng.yf.aspect;

import com.yufeng.yf.common.utils.JsonUtils;
import com.yufeng.yf.common.web.aspect.BaseLogAspect;
import javassist.*;
import javassist.bytecode.CodeAttribute;
import javassist.bytecode.LocalVariableAttribute;
import javassist.bytecode.MethodInfo;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Arrays;
import java.util.HashMap;

/**
 * AOP的资料：
 * http://www.cnblogs.com/-bumblebee-/archive/2012/03/29/2423408.html
 * http://hotstrong.iteye.com/blog/1330046
 * http://blog.csdn.net/confirmaname/article/details/9728327
 * http://blog.csdn.net/z2007130205/article/details/25713843
 *
 * @author gv
 */

@Aspect
@Component
public class AppControllerLogAspect extends BaseLogAspect {

    /**
     * 环绕通配符方式
     * @param pjp
     * @return
     * @throws Throwable
     */
    @Override
    @Around("execution(* com.yufeng.yf.controller.*Controller.*(..))")
    public Object aroundControllerMethod(final ProceedingJoinPoint pjp) throws Throwable {
        super.logBefore(pjp);
        Object returnVal = pjp.proceed();
        LOG.info("3.返回值：\n" + JsonUtils.obj2StringPretty(returnVal));
        super.logAfter();
        return returnVal;
    }

}
