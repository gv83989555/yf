package com.yufeng.yf.controller;


import com.yufeng.yf.app.pojo.AppParamExample;
import com.yufeng.yf.common.web.entity.ServerResponse;
import com.yufeng.yf.app.service.AppParamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
public class TestController {

    @Autowired
    AppParamService appParamService;

    @GetMapping("/test")
    public Object test() {
        HashMap<String, Object> result = new HashMap<>(2);
        result.put("hello", appParamService.selectByExample(new AppParamExample()));
        return ServerResponse.createBySuccess(result);
    }


    @RequestMapping(value = "tran", method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse tran() {
        appParamService.transactionalTest2();
        return ServerResponse.createBySuccess();
    }


}
