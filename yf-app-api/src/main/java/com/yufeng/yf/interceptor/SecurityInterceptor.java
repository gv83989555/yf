package com.yufeng.yf.interceptor;

import com.yufeng.yf.common.redis.RedisClient;
import com.yufeng.yf.common.utils.JsonUtils;
import com.yufeng.yf.common.utils.RequestUtil;
import com.yufeng.yf.common.web.entity.ServerResponse;
import com.yufeng.yf.common.web.enums.BaseResponseCode;
import com.yufeng.yf.common.web.exception.ParamException;
import com.yufeng.yf.common.web.exception.TokenErrorException;
import com.yufeng.yf.common.web.exception.TokenNullException;
import com.yufeng.yf.support.CurrentUser;
import com.yufeng.yf.support.HttpContext;
import com.yufeng.yf.support.HttpContextThreadLocal;
import com.yufeng.yf.support.HttpRequestHeader;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author 高巍
 * @createTime 2018年09月01日 09:35
 * @description 安全拦截器
 */
@Component
public class SecurityInterceptor extends HandlerInterceptorAdapter {
    private static final Logger logger = LoggerFactory.getLogger(SecurityInterceptor.class);

    @Autowired
    RedisClient redisClient;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String requestUrl = request.getRequestURL().toString();
        String requestUri = request.getRequestURI();
        logger.info("SecurityInterceptor current request url  : [{}]  start", requestUrl);
        PrintWriter out = null;
        String res = null;
        try {
            //http请求上下文
            HttpContext httpContext = new HttpContext();
            //构建请求头参数
            HttpRequestHeader requestHeader = this.buildRequestHeader(request);
            //构建当前用户
            CurrentUser currentUser = this.buildCurrentUser(requestHeader.getToken());
            httpContext.setRequestHeader(requestHeader);
            httpContext.setCurrentUser(currentUser);
        } catch (TokenNullException e) {
            out = this.getPrintWriter(response);
            res = JsonUtils.objectToJson(ServerResponse.createByErrorCodeMessage(BaseResponseCode.TOKEN_NULL));
            out.print(res);
            this.closePrintWriter(out);
            logger.error("SecurityInterceptor current request url  : [{}]  fail :{}", requestUrl, res);
            return false;
        } catch (ParamException e) {
            out = this.getPrintWriter(response);
            res = JsonUtils.objectToJson(ServerResponse.createByErrorCodeMessage(BaseResponseCode.ILLEGAL_HEADER_ARGUMENT));
            out.print(res);
            this.closePrintWriter(out);
            logger.error("SecurityInterceptor current request url  : [{}]  fail :{}", requestUrl, res);
            return false;
        } catch (TokenErrorException e) {
            out = this.getPrintWriter(response);
            res = JsonUtils.objectToJson(ServerResponse.createByErrorCodeMessage(BaseResponseCode.TOKEN_ERROR));
            out.print(res);
            this.closePrintWriter(out);
            logger.error("SecurityInterceptor current request url  : [{}]  fail :{}", requestUrl, res);
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            out = this.getPrintWriter(response);
            res = JsonUtils.objectToJson(ServerResponse.createByErrorCodeMessage(BaseResponseCode.SERVER_ERROR));
            out.print(res);
            this.closePrintWriter(out);
            logger.error("SecurityInterceptor current request url  : [{}]  fail :{}", requestUrl, res);
            return false;
        }
        return true;
    }

    /**
     * token获取当前用户信息
     *
     * @param token
     * @return
     */
    private CurrentUser buildCurrentUser(String token) {
        //todo ... 自行实现
        long start = System.currentTimeMillis();
//        Object o = redisClient.get(token);
        logger.info("----缓存读取用户信息耗时：{}", System.currentTimeMillis() - start);
//        if (o == null) {
            //查询数据库

            //数据库不存在，抛出TokenErrorException异常
//        }
        //重置缓存过期时间

        //构建当前用户信息
        CurrentUser currentUser = new CurrentUser();
        currentUser.setUserId(999L);
        return currentUser;
    }

    /**
     * 构建请求头参数
     *
     * @param request
     * @return
     */
    private HttpRequestHeader buildRequestHeader(HttpServletRequest request) {
        String versionName = request.getHeader("versionName");
        String versionCode = request.getHeader("versionCode");
        String channel = request.getHeader("channel");
        String source = request.getHeader("source");
        String token = request.getHeader("token");
        //验证参数判断
//        if (StringUtils.isBlank(versionName) ||
//                StringUtils.isBlank(versionCode) ||
//                StringUtils.isBlank(channel) ||
//                StringUtils.isBlank(source)) {
//            throw new ParamException("请求头部参数遗漏");
//        }
        //验证token
//        if (StringUtils.isBlank(token)) {
//            throw new TokenNullException("token 没有传递");
//        }
        HttpRequestHeader requestHeader = new HttpRequestHeader();
        requestHeader.setChannel(channel);
        requestHeader.setSource(source);
        requestHeader.setVersionCode(StringUtils.isNotBlank(versionCode) ? Integer.valueOf(versionCode) : 0);
        requestHeader.setVersionName(versionName);
        requestHeader.setToken(token);
        requestHeader.setRequestIp(RequestUtil.getIpAddr(request));
        return requestHeader;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        //清空请求data数据
        HttpContextThreadLocal.clean();
    }

    /**
     * 关闭流
     */
    private void closePrintWriter(PrintWriter out) {
        out.flush();
        out.close();
    }

    /**
     * 重置response 获取输出流
     */
    private PrintWriter getPrintWriter(HttpServletResponse response) throws IOException {
        //这里要添加reset，否则报异常 getWriter() has already been called for this response.
        response.reset();
        //这里要设置编码，否则会乱码
        response.setCharacterEncoding("UTF-8");
        //这里要设置返回值的类型，因为全部是json接口。
        response.setContentType("application/json;charset=UTF-8");
        return response.getWriter();
    }
}
