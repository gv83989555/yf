package com.yufeng.yf.support;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author GV
 * Date:2018/4/22
 * Time:上午9:38
 */

public class BaseController {
    protected Logger logger = LoggerFactory.getLogger(this.getClass());

    protected CurrentUser getCurrentUser(){
        HttpContext httpContext = HttpContextThreadLocal.getCurrentContext(false);
        return httpContext.getCurrentUser();
    }
}
