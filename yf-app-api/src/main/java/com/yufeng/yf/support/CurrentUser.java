package com.yufeng.yf.support;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 高巍
 * @createTime 2018年09月01日 09:51
 * @description 当前访问用户
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CurrentUser {
    private Long userId;
    //... ...
}
