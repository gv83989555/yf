package com.yufeng.yf.support;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author gv
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class HttpContext {
    private HttpRequestHeader requestHeader;
    private CurrentUser currentUser;
}
