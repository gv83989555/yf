package com.yufeng.yf.support;


/**
 * @author gv
 */
public class HttpContextThreadLocal {
    /**
     * 静态变量,作为各个线程中的Map的key，每个HttpContext实例为相应的value
     */
    private static final ThreadLocal<HttpContext> CURRENT_CONTEXT = new ThreadLocal<HttpContext>();

    /**
     * 获取ThreadLocal中当前线程对应的HttpContext实例
     * @param throwFlag set方法传入false,当context为空时new一个新实例;get方法传入true,当context为空时throw异常
     * @return
     */
    public static HttpContext getCurrentContext(boolean throwFlag) {
        HttpContext context = CURRENT_CONTEXT.get();
        if (context == null) {
            if (throwFlag) {
                throw new RuntimeException("context can not be access now");
            } else {
                context = new HttpContext();
            }
        }
        return context;
    }

    public static void setCurrentContext(HttpContext httpContext){
        CURRENT_CONTEXT.set(httpContext);
    }
    public static void clean(){
        CURRENT_CONTEXT.remove();
    }
}
