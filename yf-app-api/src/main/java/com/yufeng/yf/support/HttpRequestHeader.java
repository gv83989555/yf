package com.yufeng.yf.support;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @author gv
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class HttpRequestHeader {
    private Integer versionCode;
    private String versionName;
    private String source;
    private String channel;
    private String token;
    private String requestIp;

}
