package com.yufeng.yf;

import com.yufeng.yf.app.pojo.AppParam;
import com.yufeng.yf.app.pojo.AppParamExample;
import com.yufeng.yf.common.mapper.custom.AppParamCustomMapper;
import com.yufeng.yf.common.redis.RedisClient;
import com.yufeng.yf.app.service.AppParamService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class YfAppApiApplicationTests {

    @Autowired
    AppParamService appParamService;

    @Autowired
    RedisClient redisClient;

    @Autowired
    AppParamCustomMapper appParamCustomMapper;

    @Test
    public void contextLoads() {
        List<AppParam> appParams = appParamService.selectByExample(new AppParamExample());
        System.out.println(appParams);
    }

    @Test
    public void redis() {
        HashMap map = (HashMap) redisClient.get("APP:PARAM");
        System.out.println(map);
    }

    @Test
    public void customMapper() {
        AppParam appParam = appParamCustomMapper.getAppParamById(1);
        appParam.setParamId(null);
        appParamCustomMapper.insert(appParam);
        System.out.println(appParam.getParamId());
    }
}
