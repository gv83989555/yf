package com.yufeng.yf.app.service.impl;

import com.yufeng.yf.app.pojo.AppParam;
import com.yufeng.yf.app.pojo.AppParamExample;
import com.yufeng.yf.common.mapper.AppParamMapper;
import com.yufeng.yf.common.mapper.custom.AppParamCustomMapper;
import com.yufeng.yf.common.service.annotation.BaseService;
import com.yufeng.yf.common.service.base.BaseServiceImpl;
import com.yufeng.yf.common.utils.SpringContextUtil;
import com.yufeng.yf.app.service.AppParamService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
* @author GV
* on 2018/08/31 14:15:29
* AppParamService接口实现类
*/

@org.springframework.stereotype.Service
@BaseService
public class AppParamServiceImpl extends BaseServiceImpl<AppParamMapper, AppParam, AppParamExample> implements AppParamService {

    private static Logger LOGGER = LoggerFactory.getLogger(AppParamServiceImpl.class);

    @Autowired
    private AppParamMapper appParamMapper;


    @Autowired
    private AppParamCustomMapper appParamCustomMapper;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void transactionalTest1(){
        AppParam appParam = appParamCustomMapper.getAppParamById(1);
        appParam.setParamId(null);
        int insert = appParamCustomMapper.insert(appParam);
        //异常回滚
        if (insert!=0){
            throw  new RuntimeException("异常回滚");
        }
        System.out.println(appParam.getParamId());
    }

    @Override
    public void transactionalTest2(){
        System.out.println(this);
        //this.transactionalTest1(); //因为调用的是target对象，不会回滚

        //通过容器获取对象为代理对象，可以回滚
        AppParamServiceImpl bean = SpringContextUtil.getBean(this.getClass());
        System.out.println(bean);
        bean.transactionalTest1();

        //通过代理工具获取代理对象，调用增强后的事务方法，可以回滚
        //AppParamService appParamService = (AppParamService) AopContext.currentProxy();
//        System.out.println(appParamService);
        //appParamService.transactionalTest1();

    }
}