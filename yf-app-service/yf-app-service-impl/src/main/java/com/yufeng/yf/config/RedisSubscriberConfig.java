package com.yufeng.yf.config;

import com.yufeng.yf.common.redis.BaseRedisConstant;
import com.yufeng.yf.subscribe.RedisKeyExpireSubscriber;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.listener.PatternTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;

/**
 * @author 高巍
 * @createTime 2018年08月31日 16:40
 * @description redis订阅者配置
 */
@Configuration
public class RedisSubscriberConfig {

    @Autowired
    RedisKeyExpireSubscriber redisKeyExpireSubscriber;

    @Autowired
    RedisConnectionFactory redisConnectionFactory;

    @Bean
    public RedisMessageListenerContainer redisMessageListenerContainer() {
        RedisMessageListenerContainer redisMessageListenerContainer = new RedisMessageListenerContainer();
        redisMessageListenerContainer.setConnectionFactory(redisConnectionFactory);
        redisMessageListenerContainer.addMessageListener(redisKeyExpireSubscriber, new PatternTopic(BaseRedisConstant.KEY_EVENT_EXPIRED));
        return redisMessageListenerContainer;

    }
}
