package com.yufeng.yf.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author 高巍
 * @createTime 2018年08月31日 06:34
 * @description 业务层配置
 */
@Configuration
@EnableScheduling
@EnableTransactionManagement
//@EnableAspectJAutoProxy(exposeProxy = true)
@MapperScan(basePackages = "com.yufeng.yf.common.mapper")
public class ServiceConfiguration {





}
