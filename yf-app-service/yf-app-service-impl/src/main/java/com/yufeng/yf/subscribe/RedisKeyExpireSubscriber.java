package com.yufeng.yf.subscribe;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.stereotype.Component;


/**
 * @author GV
 * Date:2018/4/29
 * Time:下午3:15
 */
@Component
public class RedisKeyExpireSubscriber implements MessageListener {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public void onMessage(Message message, byte[] bytes) {
        String msg = new String(message.getBody());
        logger.info("收到消息key过期 message：{}", msg);
    }
}
