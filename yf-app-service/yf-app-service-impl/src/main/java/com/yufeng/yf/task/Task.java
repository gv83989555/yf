package com.yufeng.yf.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author 高巍
 * @createTime 2018年09月02日 21:08
 * @description 定时任务demo
 */
@Component
public class Task {

    @Value("${test}")
    private String test;

    private static Logger logger = LoggerFactory.getLogger(Task.class);

    @Scheduled(cron = "0 0/1 * * * ?")
    public void test(){
        logger.info("@Scheduled(cron = \"0 0/1 * * * ?\")");
    }


    @PostConstruct
    public void init()
    {
        logger.info("[{}]  inited test :{}  ",this.getClass().getSimpleName(),this.test);
    }
}
