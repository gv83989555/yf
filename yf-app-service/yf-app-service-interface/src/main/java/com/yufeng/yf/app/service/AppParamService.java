package com.yufeng.yf.app.service;


import com.yufeng.yf.app.pojo.AppParam;
import com.yufeng.yf.app.pojo.AppParamExample;
import com.yufeng.yf.common.service.base.BaseService;

/**
* @author GV
* on 2018/08/31 14:15:29
* AppParamService接口
*/
public interface AppParamService extends BaseService<AppParam, AppParamExample> {

    void transactionalTest1();

    void transactionalTest2();
}