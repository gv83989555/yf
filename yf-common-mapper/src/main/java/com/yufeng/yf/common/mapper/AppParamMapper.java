package com.yufeng.yf.common.mapper;

import com.yufeng.yf.app.pojo.AppParam;
import com.yufeng.yf.app.pojo.AppParamExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface AppParamMapper {
    int countByExample(AppParamExample example);

    int deleteByExample(AppParamExample example);

    int deleteByPrimaryKey(Integer paramId);

    int insert(AppParam record);

    int insertSelective(AppParam record);

    List<AppParam> selectByExample(AppParamExample example);

    AppParam selectByPrimaryKey(Integer paramId);

    int updateByExampleSelective(@Param("record") AppParam record, @Param("example") AppParamExample example);

    int updateByExample(@Param("record") AppParam record, @Param("example") AppParamExample example);

    int updateByPrimaryKeySelective(AppParam record);

    int updateByPrimaryKey(AppParam record);
}