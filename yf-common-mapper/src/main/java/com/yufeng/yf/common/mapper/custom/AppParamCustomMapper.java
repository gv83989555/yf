package com.yufeng.yf.common.mapper.custom;

import com.yufeng.yf.app.pojo.AppParam;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

/**
 * @author 高巍
 * @createTime 2018年08月31日 21:39
 * @description 自定义mapper
 */
public interface AppParamCustomMapper {

    @Select("select * from app_param where param_id=#{paramId}")
    AppParam getAppParamById(Integer paramId);

    @Options(useGeneratedKeys = true, keyProperty = "paramId")
    @Insert("insert into app_param (param_id, param_name, param_key, " +
            "param_value, state, type," +
            "remark, creator, modifier," +
            "create_time, update_time) " +
            " values (#{paramId,jdbcType=INTEGER}, #{paramName,jdbcType=VARCHAR}, #{paramKey,jdbcType=VARCHAR}, " +
            "      #{paramValue,jdbcType=VARCHAR}, #{state,jdbcType=TINYINT}, #{type,jdbcType=TINYINT}, " +
            "      #{remark,jdbcType=VARCHAR}, #{creator,jdbcType=INTEGER}, #{modifier,jdbcType=INTEGER}, " +
            "      #{createTime,jdbcType=BIGINT}, #{updateTime,jdbcType=BIGINT})")
    int insert(AppParam appParam);


}
