package com.yufeng.yf.common.pay;

/**
 * @author GV
 * Date:2018/6/19
 * Time:下午1:40
 */

public enum PayEnum {

    /** 未支付 */
    NOT_PAY(0, "NOT_PAY"),
    /** 支付成功 */
    SUCCESS(1, "SUCCESS"),
    /** 支付失败 */
    FAIL(2, "FAIL");

    private final int code;
    private final String desc;

    PayEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }


    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

}
