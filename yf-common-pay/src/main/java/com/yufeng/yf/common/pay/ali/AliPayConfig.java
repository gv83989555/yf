package com.yufeng.yf.common.pay.ali;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author GV
 * Date:2018/6/12
 * Time:上午11:16
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class AliPayConfig {
    /** 商户appid */
    private String appid;
    /** 私钥 pkcs8格式的 */
    private String rsaPrivateKey;
    /** 支付宝公钥 */
    private String alipayPublicKey;
    /** 请求网关地址 */
    public static String URL = "https://openapi.alipay.com/gateway.do";
    /** 编码 */
    public static String CHARSET = "UTF-8";
    /** 返回格式 */
    public static String FORMAT = "json";
    /** 日志记录目录 */
    public static String log_path = "/log";
    /** RSA2 */
    public static String SIGNTYPE = "RSA2";
}
