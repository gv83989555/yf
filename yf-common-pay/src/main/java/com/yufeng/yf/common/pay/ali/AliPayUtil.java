package com.yufeng.yf.common.pay.ali;

import java.math.BigDecimal;

/**
 * @author GV
 * Date:2018/6/19
 * Time:上午9:52
 */

public class AliPayUtil {
    /**
     * 分转元
     */
    public static String feeToYuan(Integer fee) {
        return (new BigDecimal(fee.doubleValue() / 100.0D)).setScale(2, 4).toPlainString();
    }
}
