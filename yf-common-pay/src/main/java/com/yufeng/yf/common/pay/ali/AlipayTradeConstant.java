package com.yufeng.yf.common.pay.ali;

/**
 * @author GV
 * Date:2018/6/19
 * Time:上午10:52
 */

public class AlipayTradeConstant {
    public static final String WAIT_BUYER_PAY = "WAIT_BUYER_PAY";
    public static final String TRADE_CLOSED = "TRADE_CLOSED";
    public static final String TRADE_SUCCESS = "TRADE_SUCCESS";
    public static final String TRADE_FINISHED = "TRADE_FINISHED";
}
