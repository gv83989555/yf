package com.yufeng.yf.common.pay.apple;

/**
 * @author GV
 * Date:2018/7/25
 * Time:下午1:53
 */

public class ApplePayConstant {

    public interface VerifyReceipt {
        String SANDBOX = "https://sandbox.itunes.apple.com/verifyReceipt";
        String BUY = "https://buy.itunes.apple.com/verifyReceipt";
    }

    public interface ResCode {
        int SUCCESS = 0;
        int SANDBOX_RECEIPT = 21007;
    }
    //Status	   描述
    //21000	App Store不能读取你提供的JSON对象
    //21002	receipt-data域的数据有问题
    //21003	receipt无法通过验证
    //21004	提供的shared secret不匹配你账号中的shared secret
    //21005	receipt服务器当前不可用
    //21006	receipt合法，但是订阅已过期。服务器接收到这个状态码时，receipt数据仍然会解码并一起发送
    //21007	receipt是Sandbox receipt，但却发送至生产系统的验证服务
    //21008	receipt是生产receipt，但却发送至Sandbox环境的验证服务
}
