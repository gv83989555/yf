package com.yufeng.yf.common.pay.apple;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author GV
 * Date:2018/7/25
 * Time:下午1:56
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AppleResEntity {
    private String environment;
    private int status;
    private ReceiptEntity receipt;
}
