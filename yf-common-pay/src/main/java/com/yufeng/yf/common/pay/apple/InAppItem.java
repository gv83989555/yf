package com.yufeng.yf.common.pay.apple;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author GV
 * Date:2018/7/25
 * Time:下午2:40
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class InAppItem {
    @JsonProperty("transaction_id")
    private String transactionId;
    @JsonProperty("original_purchase_date")
    private String originalPurchaseDate;
    private String quantity;
    @JsonProperty("original_transaction_id")
    private String originalTransactionId;
    @JsonProperty("purchase_date_pst")
    private String purchaseDatePst;
    @JsonProperty("original_purchase_date_ms")
    private String originalPurchaseDateMs;
    @JsonProperty("purchase_date_ms")
    private String purchaseDateMs;
    @JsonProperty("product_id")
    private String productId;
    @JsonProperty("original_purchase_date_pst")
    private String originalPurchaseDatePst;
    @JsonProperty("is_trial_period")
    private String isTrialPeriod;
    @JsonProperty("purchase_date")
    private String purchaseDate;

}
