package com.yufeng.yf.common.pay.apple;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author GV
 * Date:2018/7/25
 * Time:下午1:57
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ReceiptEntity {
    @JsonProperty("adam_id")
    private int adamId;
    @JsonProperty("receipt_creation_date")
    private String receiptCreationDate;
    @JsonProperty("original_application_version")
    private String originalApplicationVersion;
    @JsonProperty("app_item_id")
    private int appItemId;
    @JsonProperty("original_purchase_date_ms")
    private String originalPurchaseDateMs;
    @JsonProperty("request_date_ms")
    private String requestDateMs;
    @JsonProperty("original_purchase_date_pst")
    private String originalPurchaseDatePst;
    @JsonProperty("original_purchase_date")
    private String originalPurchaseDate;
    @JsonProperty("receipt_creation_date_pst")
    private String receiptCreationDatePst;
    @JsonProperty("receipt_type")
    private String receiptType;
    @JsonProperty("bundle_id")
    private String bundleId;
    @JsonProperty("receipt_creation_date_ms")
    private String receiptCreationDateMs;
    @JsonProperty("request_date")
    private String requestDate;
    @JsonProperty("version_external_identifier")
    private int versionExternalIdentifier;
    @JsonProperty("request_date_pst")
    private String requestDatePst;
    @JsonProperty("download_id")
    private int downloadId;
    @JsonProperty("application_version")
    private String applicationVersion;
    @JsonProperty("in_app")
    private List<InAppItem> inApp;
}
