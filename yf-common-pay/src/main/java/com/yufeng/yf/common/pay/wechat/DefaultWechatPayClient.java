package com.yufeng.yf.common.pay.wechat;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;

import java.util.HashMap;
import java.util.SortedMap;

/**
 * @author GV
 * Date:2018/6/12
 * Time:上午11:57
 */

public class DefaultWechatPayClient implements WechatPayClient {

    private WechatConfig wechatConfig;

    private static final String UNIFIEDORDER = "https://api.mch.weixin.qq.com/pay/unifiedorder";
    private static final String ORDERQUERY = "https://api.mch.weixin.qq.com/pay/orderquery";
    private static final String CLOSEORDER = "https://api.mch.weixin.qq.com/pay/closeorder";


    public DefaultWechatPayClient(WechatConfig wechatConfig) {
        this.wechatConfig = wechatConfig;
    }

    @Override
    public HashMap<String, String> unifiedorder(SortedMap<String, String> data) throws Exception {
        data.put("mch_id", wechatConfig.getMchid());
        data.put("appid", wechatConfig.getAppid());
        data.put("sign", WechatSignature.greateSign(data, wechatConfig.getAppkey()));
        return this.execute(data, UNIFIEDORDER);
    }

    @Override
    public HashMap<String, String> queryOrder(SortedMap<String, String> data) throws Exception {
        data.put("mch_id", wechatConfig.getMchid());
        data.put("appid", wechatConfig.getAppid());
        data.put("sign", WechatSignature.greateSign(data, wechatConfig.getAppkey()));
        return this.execute(data, ORDERQUERY);
    }


    /**
     * 请求下单
     */
    private HashMap<String, String> execute(SortedMap<String, String> data, String url) throws Exception {
        //http连接管理
        BasicHttpClientConnectionManager connManager = new BasicHttpClientConnectionManager(
                RegistryBuilder.<ConnectionSocketFactory>create()
                        .register("http", PlainConnectionSocketFactory.getSocketFactory())
                        .register("https", SSLConnectionSocketFactory.getSocketFactory())
                        .build(),
                null,
                null,
                null
        );
        //http客户端
        HttpClient httpClient = HttpClientBuilder.create()
                .setConnectionManager(connManager)
                .build();
        //post请求
        HttpPost httpPost = new HttpPost(url);
        RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(6000).setConnectTimeout(8000).build();
        httpPost.setConfig(requestConfig);
        StringEntity postEntity = new StringEntity(XmlUtils.mapToXml(data), "UTF-8");
        httpPost.addHeader("Content-Type", "text/xml");
        httpPost.addHeader("User-Agent", "wxpay sdk java v1.0 1502089731");
        httpPost.setEntity(postEntity);
        //发起请求获得响应数据
        HttpResponse httpResponse = httpClient.execute(httpPost);
        HttpEntity httpEntity = httpResponse.getEntity();
        return (HashMap<String, String>) XmlUtils.xmlToMap(EntityUtils.toString(httpEntity, "UTF-8"));
    }






}
