package com.yufeng.yf.common.pay.wechat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author GV
 * Date:2018/6/12
 * Time:上午11:15
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class WechatConfig {
    private String appid;
    private String mchid;
    private String appkey;
}
