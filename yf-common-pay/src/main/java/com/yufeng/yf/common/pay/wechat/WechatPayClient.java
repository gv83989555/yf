package com.yufeng.yf.common.pay.wechat;

import java.util.HashMap;
import java.util.SortedMap;

/**
 * @author GV
 * Date:2018/6/12
 * Time:下午12:01
 */
public interface WechatPayClient {

    HashMap<String,String> unifiedorder(SortedMap<String, String> data) throws Exception;

    HashMap<String,String> queryOrder(SortedMap<String, String> data) throws Exception;
}
