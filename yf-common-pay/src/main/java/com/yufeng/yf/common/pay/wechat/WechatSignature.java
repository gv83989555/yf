package com.yufeng.yf.common.pay.wechat;


import com.yufeng.yf.common.utils.MD5Util;

import java.util.Arrays;
import java.util.Map;
import java.util.Set;

/**
 * @author GV
 * Date:2018/6/14
 * Time:下午3:20
 */

public class WechatSignature {

    public static boolean isSignatureValid(Map<String, String> data, String key) {
        if (!data.containsKey("sign")) {
            return false;
        }
        String sign = data.get("sign");
        return greateSign(data, key).equals(sign);
    }

    public static String greateSign(final Map<String, String> data, String key) {
        Set<String> keySet = data.keySet();
        String[] keyArray = keySet.toArray(new String[keySet.size()]);
        //排序
        Arrays.sort(keyArray);
        StringBuilder sb = new StringBuilder();
        //遍历map拼接字符串MD5签名
        for (String k : keyArray) {
            //签名字段不参与
            if (k.equals("sign")) {
                continue;
            }
            // 参数值为空，则不参与签名
            if (data.get(k).trim().length() > 0) {
                sb.append(k).append("=").append(data.get(k).trim()).append("&");
            }
        }
        sb.append("key=").append(key);
        return MD5Util.md5(sb.toString()).toUpperCase();
    }





}
