package com.yufeng.yf.common.pay.wechat;

/**
 * @author GV
 * Date:2018/6/14
 * Time:下午1:24
 */

public class WechatTradeConstant {

    public interface ReturnCode {
        String SUCCESS = "SUCCESS";
        String FAIL = "FAIL";
    }


    public interface TradeStatus {
        /**
         * 未支付
         */
        String NOTPAY = "NOTPAY";
        /**
         * 转入退款
         */
        String REFUND = "REFUND";
        /**
         * 支付成功
         */
        String SUCCESS = "SUCCESS";
    }


    /**
     * 已关闭
     */
    public static final String TRADE_STATUS_CLOSED = "CLOSED";
    /**
     * 用户支付中
     */
    public static final String TRADE_STATUS_USERPAYING = "USERPAYING";
    /**
     * 支付错误
     */
    public static final String TRADE_STATUS_PAYERROR = "PAYERROR";

    /**
     * 回调结果
     */
    public interface NotifyResultFiled {
        String RETURN_CODE = "return_code";
        String OUT_TRADE_NO = "out_trade_no";
        String APPID = "appid";
        String MCH_ID = "mch_id";
        String TRADE_TYPE = "trade_type";
        String NOTIFY_URL = "notify_url";
        String PREPAY_ID = "prepay_id";
        String TOTAL_FEE = "total_fee";
    }


}
