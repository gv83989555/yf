package com.yufeng.yf.common.pay.yzf;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;

/**
 * @author GV
 * Date:2018/6/22
 * Time:上午11:32
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ResponseEntity {
    private Integer code;
    private String desc;
    private HashMap<String,String> data;

}
