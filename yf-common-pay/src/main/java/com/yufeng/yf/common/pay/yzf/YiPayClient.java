package com.yufeng.yf.common.pay.yzf;

import com.yufeng.yf.common.utils.JsonUtils;
import com.yufeng.yf.common.utils.RandomUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;

/**
 * @author GV
 * Date:2018/5/22
 * Time:下午3:02
 */

public class YiPayClient {

    private static final Logger log = LoggerFactory.getLogger(YiPayClient.class);

    public static ResponseEntity httpPostPay(HashMap<String, String> param, String url) throws Exception {
        return JsonUtils.jsonToPojo(execute(url, param), ResponseEntity.class);
    }

    /**
     * HTTP POST
     */
    private static String execute(String url, HashMap<String, String> param) throws Exception {
        //http连接管理
        BasicHttpClientConnectionManager connManager = new BasicHttpClientConnectionManager(
                RegistryBuilder.<ConnectionSocketFactory>create()
                        .register("http", PlainConnectionSocketFactory.getSocketFactory())
                        .register("https", SSLConnectionSocketFactory.getSocketFactory())
                        .build(),
                null,
                null,
                null
        );
        //http客户端
        HttpClient httpClient = HttpClientBuilder.create()
                .setConnectionManager(connManager)
                .build();
        //post请求
        HttpPost httpPost = new HttpPost(url);
        RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(6000).setConnectTimeout(8000).build();
        httpPost.setConfig(requestConfig);
        StringEntity postEntity = new StringEntity(JsonUtils.objectToJson(param), "UTF-8");
        httpPost.addHeader("Content-Type", "application/json");
        httpPost.setEntity(postEntity);

        //发起请求获得响应数据
        HttpResponse httpResponse = httpClient.execute(httpPost);
        log.info("响应状态：{}", httpResponse.getStatusLine());
        HttpEntity httpEntity = httpResponse.getEntity();
        return EntityUtils.toString(httpEntity, "UTF-8");
    }


    public static void main(String[] args) {
        HashMap<String, String> param = new HashMap<>();
        param.put("agentCode", YiPayConfig.AGENT_ID);
        param.put("merchantCode", YiPayConfig.MERCHANT_CODE);
        param.put("downOrderNum", RandomUtils.createOrderNumber(Integer.parseInt(RandomUtils.randomCode(2))));
        param.put("totalAmount", "2.00");
        param.put("channelCode", "M001");
        param.put("interfaceType", YiPayConstant.InterfaceType.TRANSFER.getCode());
        param.put("payType", YiPayConstant.PayType.ALI_PAY.getCode());
        param.put("goodsName", "测试商品");
        param.put("describe", "描述不能为空");
        param.put("ip", "125.115.190.230");
        param.put("callBackUrl", "http://www.baidu.com/notify");
        param.put("successUrl", "http://www.baidu.com");
        param.put("extendedField", "咩有扩展");
        param.put("sign", YiSignature.greateSign(param.get("totalAmount"), param.get("downOrderNum"), YiPayConfig.SALT));
        System.out.println(param);
        try {
            ResponseEntity responseEntity = httpPostPay(param, YiPayConfig.PAY_URL);

            System.out.println(responseEntity);
            System.out.println(responseEntity.getData());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //a8179324644b8bce18f1aeb0da9e701d


}
