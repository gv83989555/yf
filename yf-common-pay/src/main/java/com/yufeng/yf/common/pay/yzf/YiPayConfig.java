package com.yufeng.yf.common.pay.yzf;

/**
 * @author GV
 * Date:2018/5/22
 * Time:下午3:04
 */

public class YiPayConfig {
    /**
     * 支付请求地址
     */
    public static final String PAY_URL = "https://mp.youqi99.cn/api/Pay";
    /**
     * 查询订单地址
     */
    public static final String QUERY_URL = "https://mp.youqi99.cn/api/PaySearch";
    /**
     * 合作方ID
     */
    public static final String AGENT_ID = "aa696addc802455094ff38f6caaaa967";
    /**
     * 商户号
     */
    public static final String MERCHANT_CODE = "5462361b843e4f0f8067e6c8c425f805";

    public static final String SALT = "971d8d55042a4401b6c2d324051a7796";
}
