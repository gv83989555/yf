package com.yufeng.yf.common.pay.yzf;


/**
 * @author GV
 * Date:2018/6/21
 * Time:下午5:21
 */

public interface YiPayConstant {
    String WAIT_BUYER_PAY = "WAIT_BUYER_PAY";
    String TRADE_CLOSED = "TRADE_CLOSED";
    String TRADE_SUCCESS = "TRADE_SUCCESS";
    String TRADE_FINISHED = "TRADE_FINISHED";

    int SUCCESS_CODE = 20000;


    enum PayType {
        ALI_PAY("T001", "支付宝"),
        WECHAT_PAY("T002", "微信"),
        QQ_PAY("T003", "QQ"),
        UNIONPAY("T004", "银联"),
        QUICK_PAY("T005", "快捷"),
        UNIONPAY_GATEWAY("T006", "银联网关");

        private final String code;
        private final String desc;

        PayType(String code, String desc) {
            this.code = code;
            this.desc = desc;
        }

        public static PayType codeOf(String code) {
            for (PayType payType : values()) {
                if (payType.getCode().equals(code)) {
                    return payType;
                }
            }
            throw new RuntimeException("没有找到对应的枚举");
        }

        public String getCode() {
            return code;
        }

        public String getDesc() {
            return desc;
        }
    }

    enum InterfaceType {
        SCAN("P001", "支付宝"),
        BARCODE("P002", "微信"),
        PUBLIC_NUM("P003", "QQ"),
        QUERY("P004", "查询"),
        ONE_CODE("P005", "一码付"),
        H5_PAY("P006", "h5支付"),
        TRANSFER("P007", "转账"),
        PC_QUICK("P008", "PC快捷");

        private final String code;
        private final String desc;

        InterfaceType(String code, String desc) {
            this.code = code;
            this.desc = desc;
        }

        public String getCode() {
            return code;
        }

        public String getDesc() {
            return desc;
        }
    }

}
