package com.yufeng.yf.common.pay.yzf;

import java.math.BigDecimal;

/**
 * @author GV
 * Date:2018/6/22
 * Time:上午9:59
 */

public class YiPayUtil {
    /**
     * 分转元
     */
    public static String feeToYuan(Integer fee) {
        return (new BigDecimal(fee.doubleValue() / 100.0D)).setScale(2, 4).toPlainString();
    }
}
