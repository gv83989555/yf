package com.yufeng.yf.common.pay.yzf;


import com.yufeng.yf.common.utils.MD5Util;

public class YiSignature {

    public static boolean isSignatureValid(String totalAmount, String downOrderNum, String salt, String sign) {
        return greateSign(totalAmount, downOrderNum, salt).equals(sign);
    }

    public static String greateSign(String totalAmount, String downOrderNum, String salt) {
        return MD5Util.md5(salt + totalAmount + downOrderNum).toLowerCase();
    }

}
