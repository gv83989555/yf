package com.yufeng.yf.common.redis;

/**
 * @author 高巍
 * @createTime 2018年08月31日 16:45
 * @description 基本redis常量
 */
public class BaseRedisConstant {
    public static final String  KEY_EVENT_EXPIRED="__keyevent@*__:expired";

}
