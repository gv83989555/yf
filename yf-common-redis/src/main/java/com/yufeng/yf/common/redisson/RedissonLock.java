package com.yufeng.yf.common.redisson;

import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * @author 高巍
 * @createTime 2018年08月31日 16:45
 * @description 基本redis常量
 */
@Component
public class RedissonLock {

    @Autowired
    private RedissonClient redissonClient;

    private RLock getLock(String key) {
        return redissonClient.getLock(key);
    }

    public void lock(String key) {
        getLock(key).lock();
    }

    public void lock(String key, Integer seconds) {
        getLock(key).lock(seconds, TimeUnit.SECONDS);
    }

    public void unlock(String key) {
        getLock(key).unlock();
    }

    public boolean tryLock(String key) {
        return getLock(key).tryLock();
    }

    public boolean tryLock(String key, Long millisecond) {
        try {
            return getLock(key).tryLock(millisecond, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean isLocked(String key) {
        return getLock(key).isLocked();
    }

}
