package com.yufeng.yf.common.utils;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/**
 * AES加解密工具类
 * Created by shuzheng on 2017/2/5.
 */
public class AESUtil {

    private static final String ENCODE_RULES = "yj_aes=19090@#$@%@#";

    /**
     * 加密
     * 1.构造密钥生成器
     * 2.根据ecnodeRules规则初始化密钥生成器
     * 3.产生密钥
     * 4.创建和初始化密码器
     * 5.内容加密
     * 6.返回字符串
     */
    public static String aesEncode(String content) {
        try {
            //1.构造密钥生成器，指定为AES算法,不区分大小写
            KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
            //2.根据ecnodeRules规则初始化密钥生成器
            //生成一个128位的随机源,根据传入的字节数组
            SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
            random.setSeed(ENCODE_RULES.getBytes());
            keyGenerator.init(128, random);
            //3.产生原始对称密钥
            SecretKey originalKey = keyGenerator.generateKey();
            //4.获得原始对称密钥的字节数组
            byte[] raw = originalKey.getEncoded();
            //5.根据字节数组生成AES密钥
            SecretKey key = new SecretKeySpec(raw, "AES");
            //6.根据指定算法AES自成密码器
            Cipher cipher = Cipher.getInstance("AES");
            //7.初始化密码器，第一个参数为加密(Encrypt_mode)或者解密解密(Decrypt_mode)操作，第二个参数为使用的KEY
            cipher.init(Cipher.ENCRYPT_MODE, key);
            //8.获取加密内容的字节数组(这里要设置为utf-8)不然内容中如果有中文和英文混合中文就会解密为乱码
            byte[] byteEncode = content.getBytes("utf-8");
            //9.根据密码器的初始化方式--加密：将数据加密
            byte[] byteAES = cipher.doFinal(byteEncode);
            //10.将加密后的数据转换为字符串
            //这里用Base64Encoder中会找不到包
            //解决办法：
            //在项目的Build path中先移除JRE System Library，再添加库JRE System Library，重新编译后就一切正常了。
            String aesEncode = new String(new BASE64Encoder().encode(byteAES));
            //11.将字符串返回
            return aesEncode;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        //如果有错就返加nulll
        return null;
    }

    /**
     * 解密
     * 解密过程：
     * 1.同加密1-4步
     * 2.将加密后的字符串反纺成byte[]数组
     * 3.将加密内容解密
     */
    public static String aesDecode(String content) {
        try {
            //1.构造密钥生成器，指定为AES算法,不区分大小写
            KeyGenerator keygen = KeyGenerator.getInstance("AES");
            //2.根据ecnodeRules规则初始化密钥生成器
            //生成一个128位的随机源,根据传入的字节数组
            SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
            random.setSeed(ENCODE_RULES.getBytes());
            keygen.init(128, random);
            //3.产生原始对称密钥
            SecretKey originalKey = keygen.generateKey();
            //4.获得原始对称密钥的字节数组
            byte[] raw = originalKey.getEncoded();
            //5.根据字节数组生成AES密钥
            SecretKey key = new SecretKeySpec(raw, "AES");
            //6.根据指定算法AES自成密码器
            Cipher cipher = Cipher.getInstance("AES");
            //7.初始化密码器，第一个参数为加密(Encrypt_mode)或者解密(Decrypt_mode)操作，第二个参数为使用的KEY
            cipher.init(Cipher.DECRYPT_MODE, key);
            //8.将加密并编码后的内容解码成字节数组
            byte[] byteContent = new BASE64Decoder().decodeBuffer(content);
            /*
             * 解密
             */
            byte[] byteDecode = cipher.doFinal(byteContent);
            String aesDecode = new String(byteDecode, "utf-8");
            return aesDecode;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            throw new RuntimeException("兄弟，配置文件中的密码需要使用AES加密，请使用AESUtil工具类修改这些值！");
            //e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        }
        //如果有错就返加nulll
        return null;
    }

    public static void main(String[] args) {
//        String[] keys = {
//                "123456", "yj_redis"
//        };
//        System.out.println("key | AESEncode | AESDecode");
//        for (String key : keys) {
//            System.out.print(key + " | ");
//            String encryptString = aesEncode(key);
//            System.out.print(encryptString + " | ");
//            String decryptString = aesDecode(encryptString);
//            System.out.println(decryptString);
//        }
//        System.out.println(aesDecode(
//                "xEF4y2fP7Yhf9SAmSzp0Aj4JIGy24qCTkZNJUHjZEq450nk6Sx2q7Nkon2UlU6LA+ouGICGjlmEDSc97yJVjrWukRVcGy2J72GoKTljlYE7bY4Z5mexNl6PFv++x+JdAY/TXCDkZwV3EY4uxEUYGyNFS 7Is9ZHWdXJ3rWpH506e0+aPFdupqkh7lo2HiIa+qpy3CEXrqqckhGUQMOhE/JHB8Daz1Sk6/Bqp6 KUC6jg5FT/rQaHYgVUbzGghJhUvq/YYJ7vbRwGUU4xwJJNc33QzVrFDzXepCGuCqFA9XF/ql2utf fcNCjStEzhfjSdZzLoLHdYeFlMgKhla+rtexjnfDLUmAHuLihFLYtB45Yixn/M43hgW8Dvdgzw4a eayJIDhY4E1qWfnMbqfqdl5hsq0W5sDNiq3EOT02jdFJJPIMHbWtMDv3EMl7DpWqXW8zKPkgRsF8 yQ76aUu3csPCZvThcOh3Um3X5golE/r5jaSuJy5OIN5DAlcBBrdbOlr+/El+LjkxHohROpuHpxiH cMi1xKdawrq0b1XgMNb6w4HryIjK0ejZIPnmS6HtsOkRA34RE2RaQ/xtrVMU6VRFlO75oqaHOZcn cVpbfOs2Ir1nZQ78Am5IeEITE8vvKXQHce9AczceuR6on/myBx3ZAWl7f0XzVGXZ0uCmyMxC1S2t Rd0ws6B9Vh3GuWfyC10Rg2XYCailONLvPMrks0oFmg8OXJFx4GLBI6oZ6JHaotIPAVFFgMkx9x56 1qNbhSCQqqFJoXqOjhn8CYMdqVcyS7Tz9SpEl74F0fqPjVk/hqR8FVM5udfxvBzZYCIJTgWxqPY0 G+os56IJqX1yRm+DY6elhggcy2ifZ8Z8UaiWPxVx06/o1lbN1qVABulyW1J6RHwWXP4wsP23uHk4 XyBjFDMZyfXs1o/y7gBP1bnmNrFnpdbDnEfQk1JWJjDG5de7aPv5iK7CwZ1NHdEz1SbsO5P3Z2CZ IFUWs1T2twtSRIm5NHgYeeCkUE/XUY8KTLxiHxk+gOY4dqSJ4JgKDxaMFafT1xUvqytyh/xKFWIn vH6rdsUt+ZEFJqM5ko+mUlY8e6RNwzKf07oROSG7nBkWzQ2EqZ/tDPdv/fngcpjTAulhteiowfdn r+SDXQaJjHbXpbfxkAtDN6wZ17linDDH5fUBMx+A9hS2jjyzX9N4fS6whpVd4oFtV39XJF9oySGJ d8EIsKV0g9vMfczyP92Mml8GEMts5RZOz9yYHvQhKgaRpJW5K1nXg4fFFDwdwXFWNuSvPySKgKTI lzPaUhfUt4YW22N1kuCl1mMBSI2XZk+gvhnFN7A+0SvQKgT+Uwm9FpUXHTJSkXoCEqzTLzTf21tk S9T1QmCwE+blP3+U7lksMQZwpIjREbGksBx6Ocl1VWKt/5Be8dQ+VWoXOo4J4oVKOhZ5fCRPlqKh TZjlx1U1LMhdBo4g5nOMnp+IGJGO3bC6Qu6dy9OgCdUYaz+IYyYn03ch4FZGIq4+9Xc6PeyuskES gAoUvLHLj846ms09I+vuRwqMRICdFC1DCaTlRmUyyId+pTrner9S69hfL1PbdGIDhmgbksUZBBJq rmYQ9xo41Adkjkx5CpH/5H9z1ndwjwL37rC9XuN+9xsJz3Hcjh+aKKTen/Rx/ZerhwxJhB4N4oiZ Ph+Fq7wJISKoFqzH9AEtY1qucOpRv/BYfcrq0sserkoWjEg4X6TOuNT9nGUDzb012SNWk6F9ffbR ICx2fGrw/7pTn4W/OPNW79gx27zYFFgfh+Dkpp9eCjZnyrN2qJ8BAJJpRVymxkssLFJS270vV+KZ HBU6NwXKNk5XMi8//U7IsYZyZ2KfRS/YkdCpD3sBHP76c0fOccgDKT1PGjGhTKbxwPwiH5vTDhKx S0k4gJiOU04ZhmAY/QwGmbNsSO9LAA2sAAuVdldE78LpwtR0GDzfbrMx7p5aEqxJ3aLP5JEL76gY I6yDNi9dKUobGzOnxH4Ju6GD1d9UnkgfNXOan8TB/vUsur6qCuSNh0h01glYFm2UqoMWnhviUJEu IPwOUiLbivwdWf7ftHpIT6OCb8L+QGMZqzKbwT+c3plZY+EstUaGM8oFmrN5oKGOw+61tQmVbrB7 Hd0MiWH8SNnOhpGTfWjGPsO27YvarQ9Bu5jct7wHUXSMNWLZ")
//                .equals(
//                        "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCaiuxiyiIFCO6u3Bv7hJmQ9vYeT23IW0pAJUs4ZLHdUo+GPMluop9V+6Ihodp5BYSaipTyaUGTI4WJiJvYsao036xINMYbRSPMVRFTKC8gMc3o+O7U+xT1uHyqoYRgf48C1N8oyCvDxxvQOIst5QwTbVungEC3WyHQuNeLHCWU3Zl6eejeaXHZLYBWN4f0jffMNt9l/y0NdXjCXJv3LLxFWQ8ahC9uu6Hu1BdalAblwv3uQrTDPFXb4TeeWUXoL+6frfpV/NVBdDD1eebfAMaUF06VMSj75W6z6RaJ0IAsOz62cW3PjvvraSyw0Z/w2JK2IoZWffkCO5W4nXaSKDUzAgMBAAECggEAfjIrTZBZr1rX7Rn4D67755CC8cVSlGVXgIHzpp0zUNN2N9uzcRCCpCfTATu1t1V2LuwzLLAeyBQdm21eNdj+cCal/GB71RDkZpqghxMtSFDjH/cGsAxH1U9NHdi+zD3dfiZuBFNQyZHxeFpz7Io5DjQiv+jO7+sO9e26tPHLDn+O6FI/xnAsXf0fycdMrjoNT46cCfCZeALf5PmVVY+rH1v7aYsZqRgRlXT+rdLqfAcZnd2XUYW3bnwIg/Gx8CngOgNSdMrhNyb5RWCDFW4t6BHT6b+FvH2nG5WnnSPXipuYPJZec0GcjuFKjADkdAcsJ9p2Bp1UWRwFn87DsLgGgQKBgQDgzSdifMe9R6WxR7FLa4c877iwnC5j2y00UVJ/LyX027zm65XUUM6xnMnBQpbfb0Dmwja9O3hCWzqe2RNpc1y5Q4g3/nNGP+drfQnCnxmCfkAUm3r2NEjMc1+bwYpdqFCV46WBDfvQuDajPor3l0rrQnTzu8Nlmqb+VxoWGTbFYwKBgQCv/ZX+GjjarIqOQOjmsNUAbTAng736xVkgEVJkMf0EYS0Mi72HUQtoOpacQ/PG26ocqLYpjVQ/83DkH0o74+Hh+PTjMf03g3jUy6UzAxtt/4f9oJrvKJGw2IS4DoADaPq86ertYTuIpn4rKikwOCCRUWVK9bddvWp2mrkvc9EB8QKBgQCAfWJ8jZvRGWmgabzxwdU7xD1JXQu68aTl2XGQ1o8YZJyVVKSABWwO4vICr5/yt/FdtDZ7dMlpDCH2d1b8F/8wuWPbtWmIBrCspHx2rF91UdoorTHcFchia7ODIF2I6NKeyc5odGixRQmdxvrBR81/qK2WPoURxxKaH4j+hozixwKBgQCjOL+Z8dv1A71lfdID0tFaGeplW4whPlMQqMAPaUVOgIn1bs0S6Rl5S41P1L4NGsHE3rxEAW23+iIjkNmafhmLKQiGhmBg0jCIu261xxXziOEpW4EWejvVFprQ0bHsd7rE5vYZvuomLLQreGjG7edWn3Ugdo/gAsLwyUMjPXkFIQKBgFsrjJqZsuDXBM2ub8b2imSrxbBjYjmS6bIJLYbhF6FLox0dJeAZoh+MvUdk7hI7ZiudW8p9HeF1a1lijvGVLV55+vthsLr4Z54SpjVJVYuxPJExNMtlXpW85wD8AYzpTg4MMvjgNyQGDLPWy+BzVlC7lLEKlf2Lv456W8FCHl83"));
//
//
        System.out.print(aesEncode("MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCL9Al+X1HqFPz+3O2El0Uf8UghjkUHgjyo0TNWXs8ZuVshJVGk5V2eOwLZxz1r51JW5/kxueSNb19bsM20u2utSVK2lTdBOp0nZPMZO4e7gy5anpGc1v1eFaLSEKffdgFdWKoBuvVGmTwChh5MQFE0fcC1k/lDeeokjgG0e1KQm6C5hhTo7E00dpW3n+MdnH4AowN9N4rU3fxJRaeLX4v98wEzzpJiyj539D9T8+ZYnlQw70/WCeNotOAFVu7gEHFRCK+xnkQhIMvfCX9GAIKVZxjeCq1FbtpjwDQezArE3iUIpeZpFeZkXZT+QaEVNTfU/ewBzKbbFddwUMpSd9ClAgMBAAECggEAeRp01JubAFmlCs0V4rQbJqbc6dx4MyUpZFOSftyPjQztpGLsoSmwtukNcHGN3r9OatFUuwjGkvSaqXb2qbu8UzitkDzSkWks8BfUnDL9Iu9CvzuTeQo4OJ+0Qt4SxbURPTsp8VFcSPj0CqMPuJ+3JRrkFLu06ASo7XTMWOaV7XbNiW0xbk/lMnEaqObbG0mSEaJka13YvsmuBZLgsvyNSaEoGtUvmFOVlzLBgrF/M8AMxIogZcYpSv7AKEL3QhIc3NBfigvNP7sYE4E9+G+ZdvMfczMn04CnjW3MBadWjZjx7ty3uL6p4TIPPAGm9KTO0TOBS+YL+qi0xlP7ZI7NSQKBgQDt/1ixl/+6Z+WTTpV1OK+CG0ZgHqHAQPmgLQ3qrArvSMz/Akst1dKqGfeDmlSnWeZm4pM9uT4WkRB8XyWEBrJYtzMyL0cQYOQ013pJA0MOw/CDZRNYB7HXkFu76VRPGo5+PS0rzeWjgs26iIqjS8oSUzE1bawEhJo1oCMIa+cvDwKBgQCWiiJJxutPyfJkyJvCXPtw+s1dzDvU+b1IAeMKjaLQl8uQOBSBEIOzVwr3xoBC2DBM14PRirkm11XBfFdkmMWzodrHgQHlZUh7MTqAPaQVRTtK6iSv3XwHaWuPMEd+UnnoEik0SMRAdk4PwQ1/PlimV5TaMHT+90aFR2CNaoqFCwKBgGB2pbldtU4VfGynZlabwi9G5NBiugbVjxgICuL6FniEFmqpwFT0nK1LVxIlRHiMLKG45hsXcP3ukDMI0YjgufrRTlPh67jMvNJaH/tl8hETA/INzrhYbhHENhpE6WIijmzGWP+vNYt/oekgHKkxN9cTlxdju1wdbu+bV3e7XHmrAoGAAVrxABUIXUmGIx4jONa5dMTtQGDsUPiGmbVao+euhY44SO4YUSki5mRsNtrUoMVteeOezi4fiJf7r6g86Szu6rUUUSN6LuSAiIti6XCkn4RmCkCdX+/wg+iRDVLz8dEwQ8SWFmBpU+M8xKuKQZEAH08+idBR3VHAksaXue4xnQ0CgYEAijOSyZM+KF1Qo54JenGIOSs8C1e5hkeuiQ3M6jDdorxWYY/TImt6OhpiQO+UkPNB5RnLUt+jK3ywunOHQtj90TqaIJMxjs/7AQ5XCnNSNBiGAwz2cungItXtDJNLaSf79Phb0eJxD47z+BHq7o/pMbcevExOue6UWv3FybViU3c="));
    }

}
