package com.yufeng.yf.common.utils;

import java.text.DecimalFormat;

/**
 * @author GV
 * Date:2018/4/25
 * Time:下午2:11
 */

public class CommonUtil {
    //地球半径
    private static double EARTH_RADIUS = 6378.137;

    private static double rad(double d) {
        return d * Math.PI / 180.0;
    }

    /**
     * 根据两点的经纬度，计算出其之间的距离（返回单位为m）
     *
     * @param lat1 纬度1
     * @param lng1 经度1
     * @param lat2 纬度2
     * @param lng2 经度2
     * @return
     */
    public static double getDistance(double lat1, double lng1, double lat2, double lng2) {
        double radLat1 = rad(lat1);
        double radLat2 = rad(lat2);
        double a = radLat1 - radLat2;
        double b = rad(lng1) - rad(lng2);
        double s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2) +
                Math.cos(radLat1) * Math.cos(radLat2) * Math.pow(Math.sin(b / 2), 2)));
        s = s * EARTH_RADIUS;
        //格式化小数
        DecimalFormat df = new DecimalFormat("0.0");
        s = Double.parseDouble(df.format((float) Math.round(s * 10000) / 10000));
        return s;
    }


}
