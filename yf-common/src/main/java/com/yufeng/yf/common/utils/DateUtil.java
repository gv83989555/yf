package com.yufeng.yf.common.utils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.chrono.IsoChronology;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 日期工具类
 */
public final class DateUtil {

    private static final Map<String, ThreadLocal<SimpleDateFormat>> timestampFormatPool = new HashMap<String, ThreadLocal<SimpleDateFormat>>();

    private static final Map<String, ThreadLocal<SimpleDateFormat>> dateFormatPool = new HashMap<String, ThreadLocal<SimpleDateFormat>>();

    private static final Object timestampFormatLock = new Object();

    private static final Object dateFormatLock = new Object();

    private static String yyyy_MM_dd = "yyyy-MM-dd";

    private static String yyyy_MM_dd_CN = "yyyy年MM月dd日";

    private static String MM_dd_CN = "M月dd日";

    private static String yyyy_MM_dd_HH_mm_ss = "yyyy-MM-dd HH:mm:ss";


    private static String timestampPattern2 = "yyyy/MM/dd HH:mm:ss";

    private static String simplifyDatePattern = "yyyyMMdd";

    private static String simplifyTimestampPattern = "yyyyMMddHHmmss";

    private static String timestampPatternCps = "yyyyMMddHHmmss";


    /**
     * 用来计算发送两次验证码之间的间隔 ()
     *
     * @param d1
     * @param d2
     * @return
     */
    public static long getBetweenSecond(Date d1, Date d2) {
        return Math.abs((d1.getTime() - d2.getTime()) / 1000);
    }


    /**
     * 获取当前的时间戳
     *
     * @return
     */
    public static Timestamp getNowTimestamp() {
        return new Timestamp(System.currentTimeMillis());
    }


    /**
     *
     */
    public static String formatTo_yyyy_MM_dd_HH_mm_ss(Date date) {
        return getTimestampFormat().format(date);
    }

    /**
     * 获取yyyy年MM月dd日
     */
    private static SimpleDateFormat getDateFormatCN() {
        ThreadLocal<SimpleDateFormat> tl = dateFormatPool.get(yyyy_MM_dd_CN);
        if (null == tl) {
            synchronized (dateFormatLock) {
                tl = dateFormatPool.get(yyyy_MM_dd_CN);
                if (null == tl) {
                    tl = new ThreadLocal<SimpleDateFormat>() {
                        @Override
                        protected synchronized SimpleDateFormat initialValue() {
                            return new SimpleDateFormat(yyyy_MM_dd_CN);
                        }
                    };
                    dateFormatPool.put(yyyy_MM_dd_CN, tl);
                }
            }
        }
        return tl.get();
    }

    /**
     * 获取yyyy年MM月dd日
     */
    private static SimpleDateFormat getDateFormat_MM_dd_CN() {
        ThreadLocal<SimpleDateFormat> tl = dateFormatPool.get(MM_dd_CN);
        if (null == tl) {
            synchronized (dateFormatLock) {
                tl = dateFormatPool.get(MM_dd_CN);
                if (null == tl) {
                    tl = new ThreadLocal<SimpleDateFormat>() {
                        @Override
                        protected synchronized SimpleDateFormat initialValue() {
                            return new SimpleDateFormat(MM_dd_CN);
                        }
                    };
                    dateFormatPool.put(MM_dd_CN, tl);
                }
            }
        }
        return tl.get();
    }

    /**
     * 获得yyyy-MM-dd
     */
    private static SimpleDateFormat getDateFormat() {
        ThreadLocal<SimpleDateFormat> tl = dateFormatPool.get(yyyy_MM_dd);
        if (null == tl) {
            synchronized (dateFormatLock) {
                tl = dateFormatPool.get(yyyy_MM_dd);
                if (null == tl) {
                    tl = new ThreadLocal<SimpleDateFormat>() {
                        @Override
                        protected synchronized SimpleDateFormat initialValue() {
                            return new SimpleDateFormat(yyyy_MM_dd);
                        }
                    };
                    dateFormatPool.put(yyyy_MM_dd, tl);
                }
            }
        }
        return tl.get();
    }

    /**
     * 获得yyyy-MM-dd HH:mm:ss
     */
    public static SimpleDateFormat getTimestampFormat() {
        ThreadLocal<SimpleDateFormat> tl = timestampFormatPool.get(yyyy_MM_dd_HH_mm_ss);
        if (null == tl) {
            synchronized (timestampFormatLock) {
                tl = timestampFormatPool.get(yyyy_MM_dd_HH_mm_ss);
                if (null == tl) {
                    tl = new ThreadLocal<SimpleDateFormat>() {
                        @Override
                        protected synchronized SimpleDateFormat initialValue() {
                            return new SimpleDateFormat(yyyy_MM_dd_HH_mm_ss);
                        }
                    };
                    timestampFormatPool.put(yyyy_MM_dd_HH_mm_ss, tl);
                }
            }
        }
        return tl.get();
    }


    private static SimpleDateFormat getSimplifyTimestampFormat() {
        ThreadLocal<SimpleDateFormat> tl = timestampFormatPool.get(simplifyTimestampPattern);
        if (null == tl) {
            synchronized (timestampFormatLock) {
                tl = timestampFormatPool.get(simplifyTimestampPattern);
                if (null == tl) {
                    tl = new ThreadLocal<SimpleDateFormat>() {
                        protected synchronized SimpleDateFormat initialValue() {
                            return new SimpleDateFormat(simplifyTimestampPattern);
                        }
                    };
                    timestampFormatPool.put(simplifyTimestampPattern, tl);
                }
            }
        }
        return tl.get();
    }

    private static SimpleDateFormat getSimplifyDateFormat() {
        ThreadLocal<SimpleDateFormat> tl = timestampFormatPool.get(simplifyDatePattern);
        if (null == tl) {
            synchronized (timestampFormatLock) {
                tl = timestampFormatPool.get(simplifyDatePattern);
                if (null == tl) {
                    tl = new ThreadLocal<SimpleDateFormat>() {
                        protected synchronized SimpleDateFormat initialValue() {
                            return new SimpleDateFormat(simplifyDatePattern);
                        }
                    };
                    timestampFormatPool.put(simplifyDatePattern, tl);
                }
            }
        }
        return tl.get();
    }

    public static SimpleDateFormat getTimestampFormatCps() {
        ThreadLocal<SimpleDateFormat> tl = timestampFormatPool.get(timestampPatternCps);
        if (null == tl) {
            synchronized (timestampFormatLock) {
                tl = timestampFormatPool.get(timestampPatternCps);
                if (null == tl) {
                    tl = new ThreadLocal<SimpleDateFormat>() {
                        protected synchronized SimpleDateFormat initialValue() {
                            return new SimpleDateFormat(timestampPatternCps);
                        }
                    };
                    timestampFormatPool.put(timestampPatternCps, tl);
                }
            }
        }
        return tl.get();
    }

    /**
     * 格式化成时间戳格式
     *
     * @param date 要格式化的日期对象
     * @return "年年年年-月月-日日 时时:分分:秒秒"格式的日期字符串
     */
    public static String timestampFormat(Date date) {
        if (date == null) {
            return "";
        }
        return getTimestampFormat().format(date);
    }

    /**
     * 格式化成时间戳格式
     *
     * @param date 要格式化的日期对象
     * @return "年年年年月月日日时时分分秒秒"格式的日期字符串
     */
    public static String simplifyTimestampFormat(Date date) {
        if (date == null) {
            return "";
        }
        return getSimplifyTimestampFormat().format(date);
    }

    /**
     * 格式化成日期格式
     *
     * @param date 要格式化的日期对象
     * @return "年年年年月月日日"格式的日期字符串
     */
    public static String simplifyDateFormat(Date date) {
        if (date == null) {
            return "";
        }
        return getSimplifyDateFormat().format(date);
    }

    /**
     * 格式化成时间戳格式
     *
     * @param date 要格式化的日期对象
     * @return "年年年年-月月-日日 时时:分分:秒秒"格式的日期字符串
     */
    public static String timestampFormatCps(Date date) {
        if (date == null) {
            return "";
        }
        return getTimestampFormatCps().format(date);
    }

    /**
     * 格式化成Unix时间戳格式
     *
     * @param date
     * @return
     */
    public static long unixTimestampFormat(Date date) {
        String unixDate = String.valueOf(date.getTime()).substring(0, 10);
        return Long.parseLong(unixDate);
    }

    /**
     * 格式化成时间戳格式
     *
     * @param datetime 要格式化的日期
     * @return "年年年年-月月-日日 时时:分分:秒秒"格式的日期字符串
     */
    public static String timestampFormat(long datetime) {
        return getTimestampFormat().format(new Date(datetime));
    }

    /**
     * 将"年年年年-月月-日日 时时:分分:秒秒"格式的日期字符串转换成Long型日期
     *
     * @param timestampStr 年年年年-月月-日日 时时:分分:秒秒"格式的日期字符串
     * @return Long型日期
     */
    public static long formatTimestampToLong(String timestampStr) {
        try {
            return getTimestampFormat().parse(timestampStr).getTime();
        } catch (ParseException e) {
            return 0L;
        }
    }

    /**
     * 将"年年年年-月月-日日 时时:分分:秒秒"格式的日期字符串转换成日期
     *
     * @param timestampStr 年年年年-月月-日日 时时:分分:秒秒"格式的日期字符串
     * @return 日期
     */
    public static Date formatTimestampToDate(String timestampStr) {
        try {
            return getTimestampFormat().parse(timestampStr);
        } catch (ParseException e) {
            return null;
        }
    }

    /**
     * 将"年年年年-月月-日日"格式的日期字符串转换成日期
     *
     * @param dateStr 年年年年-月月-日日"格式的日期字符串
     * @return 日期
     */
    public static Date yyyy_MM_dd_ToDate(String dateStr) {
        try {
            return getDateFormat().parse(dateStr);
        } catch (ParseException e) {
            return null;
        }
    }

    /**
     * 格式化成日期格式
     *
     * @param date 要格式化的日期
     * @return "年年年年-月月-日日"格式的日期字符串
     */
    public static String dateFormat(Date date) {
        if (date == null) {
            return "";
        }
        return getDateFormat().format(date);
    }


    /**
     * 格式化成日期格式
     *
     * @param date 要格式化的日期
     * @return "年年年年-月月-日日"格式的日期字符串
     */
    public static String dateFormatTo_yyyy_MM_dd(Date date) {
        if (date == null) {
            return "";
        }
        return new SimpleDateFormat(yyyy_MM_dd).format(date);
    }


    /**
     * 格式化成日期格式
     *
     * @param date 要格式化的日期
     * @return "××××年××月××日"格式的日期字符串
     */
    public static String dateFormatCN(Date date) {
        if (date == null) {
            return "";
        }
        return getDateFormatCN().format(date);
    }

    /**
     * 格式化成日期格式
     *
     * @param date 要格式化的日期
     * @return "××××年××月××日"格式的日期字符串
     */
    public static String dateformatMMDDCN(Date date) {
        if (date == null) {
            return "";
        }
        return getDateFormat_MM_dd_CN().format(date);
    }

    /**
     * 格式化成日期格式
     *
     * @param datetime 要格式化的日期
     * @return "年年年年-月月-日日"格式的日期字符串
     */
    public static String dateFormat(long datetime) {
        return getDateFormat().format(new Date(datetime));
    }

    /**
     * 将"年年年年-月月-日日"格式的日期字符串转换成Long型日期
     *
     * @param dateStr "年年年年-月月-日日"格式的日期字符串
     * @return Long型日期
     */
    public static long formatDateToLong(String dateStr) {
        try {
            return getDateFormat().parse(dateStr).getTime();
        } catch (ParseException e) {
            return 0L;
        }
    }

    /**
     * 得到本月的第一天
     *
     * @return 以"年年年年-月月-日日"格式返回当前月第一天的日期
     */
    public static String getFirstDayOfCurrentMonth() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
        return getDateFormat().format(calendar.getTime());
    }

    /**
     * 得到本月的最后一天
     *
     * @return 以"年年年年-月月-日日"格式返回当前月最后一天的日期
     */
    public static String getLastDayOfCurrentMonth() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        return getDateFormat().format(calendar.getTime());
    }

    /**
     * 获取当前日期前（后）的某一天
     *
     * @param offset 偏移量，即当前日期之前（后）多少天，如果是之前，offset为负的整数
     * @return 以"年年年年-月月-日日"格式返回要获取的日期
     */
    public static Date getDayAfterCurrentDate(int offset) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, offset);
        return calendar.getTime();
    }

    /**
     * 返回以当前时间为基准的七日的结束销售时间
     *
     * @return
     */
    public static Date getSevenDaysAfterOnSale() {
        Calendar calendarAdd = Calendar.getInstance();
        calendarAdd.setTime(new Date());

        calendarAdd.add(Calendar.DAY_OF_MONTH, 6);
        calendarAdd.set(Calendar.HOUR_OF_DAY, 23);
        calendarAdd.set(Calendar.MINUTE, 59);
        calendarAdd.set(Calendar.SECOND, 59);
        return calendarAdd.getTime();
    }

    /**
     * 根据指定的时间参数获取时间
     *
     * @return
     */
    public static Date getTimeByIdentifiedValues(Integer year, Integer month, Integer day, Integer hour,
                                                 Integer minute, Integer second) {
        Calendar calendarAdd = Calendar.getInstance();
        calendarAdd.setTime(new Date());

        calendarAdd.add(Calendar.DAY_OF_MONTH, month);
        calendarAdd.set(Calendar.HOUR_OF_DAY, day);
        calendarAdd.set(Calendar.MINUTE, minute);
        calendarAdd.set(Calendar.SECOND, second);
        return calendarAdd.getTime();
    }

    /**
     * 获取默认日期时间
     *
     * @return
     */
    public static Date getDefaultDateTime() {
        return new Date(formatTimestampToDate("1970-01-01 00:00:00").getTime());
    }


    /**
     * 获取时间为当日0点的时间戳
     *
     * @return
     */
    public static Timestamp getCurrentTimeStamp(int hour, int minute, int second) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, second);
        return new Timestamp(calendar.getTime().getTime());
    }

    /**
     * 字符串转时间戳
     *
     * @param str
     * @return
     * @throws ParseException
     */
    public static Timestamp formatStringToTimestamp(String str) {
        Timestamp timestamp = null;
        try {
            timestamp = new Timestamp(getTimestampFormat().parse(str).getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return timestamp;
    }
    //未完，待续

    public static int getMaxDayOfMonth(int year, int month) {
        int dom = 31;
        switch (month) {
            case 2:
                dom = (IsoChronology.INSTANCE.isLeapYear(year) ? 29 : 28);
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                dom = 30;
                break;
        }
        return dom;
    }

    //由出生日期获得年龄
    public static int getAge(Date birthDay) {
        try {
            Calendar cal = Calendar.getInstance();
            if (cal.before(birthDay)) {
                throw new IllegalArgumentException(
                        "The birthDay is before Now.It's unbelievable!");
            }
            int yearNow = cal.get(Calendar.YEAR);
            int monthNow = cal.get(Calendar.MONTH);
            int dayOfMonthNow = cal.get(Calendar.DAY_OF_MONTH);
            cal.setTime(birthDay);

            int yearBirth = cal.get(Calendar.YEAR);
            int monthBirth = cal.get(Calendar.MONTH);
            int dayOfMonthBirth = cal.get(Calendar.DAY_OF_MONTH);

            int age = yearNow - yearBirth;

            if (monthNow <= monthBirth) {
                if (monthNow == monthBirth) {
                    if (dayOfMonthNow < dayOfMonthBirth) {
                        age--;
                    }
                } else {
                    age--;
                }
            }
            return age < 0 ? 0 : age;

        } catch (Exception e) {
            e.printStackTrace();
            return 1;
        }
    }


}