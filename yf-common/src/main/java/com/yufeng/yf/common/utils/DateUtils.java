package com.yufeng.yf.common.utils;

import java.util.Calendar;

/**
 * @author GV
 * Date:2018/8/16
 * Time:下午3:09
 */

public class DateUtils {

    /**
     * 获取当前年与日拼接，返回整型
     */
    public static Integer getYearMonthCurrentDay() {
        Calendar date = Calendar.getInstance();
        String year = String.valueOf(date.get(Calendar.YEAR));
        String month = String.valueOf(date.get(Calendar.MONTH) + 1);
        String day = String.valueOf(date.get(Calendar.DAY_OF_MONTH));
        return Integer.parseInt(year + month + day);
    }

    /**
     * 获取当前日期向前向后的指定年月日
     */
    public static Integer getYearMonthDay(int num) {
        Calendar date = Calendar.getInstance();
        date.add(Calendar.DAY_OF_MONTH, num);
        String year = String.valueOf(date.get(Calendar.YEAR));
        String month = String.valueOf(date.get(Calendar.MONTH) + 1);
        String day = String.valueOf(date.get(Calendar.DAY_OF_MONTH));
        return Integer.parseInt(year + month + day);
    }


}
