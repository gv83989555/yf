package com.yufeng.yf.common.utils;

import java.util.Calendar;
import java.util.Date;

/**
 * @author GV
 * Date:2018/8/16
 * Time:下午2:38
 */

public class TimestampUtil {

    /**
     * 获得当日起始时间戳
     */
    public static Long getDayStartTimestamp() {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date(System.currentTimeMillis()));
        c.set(Calendar.HOUR_OF_DAY,0);
        c.set(Calendar.MINUTE,0);
        c.set(Calendar.SECOND,0);
        //当天00:00:00
        return c.getTimeInMillis();
    }
    /**
     * 获得明天起始时间戳
     */
    public static Long getTomorrowStartTimestamp() {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date(System.currentTimeMillis()));
        c.set(Calendar.HOUR_OF_DAY,0);
        c.set(Calendar.MINUTE,0);
        c.set(Calendar.SECOND,0);
        c.add(Calendar.DAY_OF_MONTH,1);
        //次日00:00:00
        return c.getTimeInMillis();

    }
}
