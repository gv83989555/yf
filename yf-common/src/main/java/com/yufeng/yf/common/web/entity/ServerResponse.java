package com.yufeng.yf.common.web.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.yufeng.yf.common.web.enums.BaseResponseCode;

import java.io.Serializable;
import java.util.Objects;

/**
 * Created by GV
 * DATE:2017/6/18
 * TIME:下午3:38
 * 保证序列化json的时候,如果是null的对象,key也会消失
 */

@JsonSerialize(include =  JsonSerialize.Inclusion.NON_NULL)
public class ServerResponse<T> implements Serializable {
    private Integer returnValue;
    private T data;
    private String msg;

    private ServerResponse(int returnValue)
    {
        this.returnValue = returnValue;
    }

    private ServerResponse(int returnValue, T data){
        this.returnValue = returnValue;
        this.data = data;
    }

    private ServerResponse(int returnValue, String msg, T data){
        this.returnValue = returnValue;
        this.msg = msg;
        this.data = data;
    }

    private ServerResponse(int returnValue, String msg){
        this.returnValue = returnValue;
        this.msg = msg;
    }

    /**
     * 使之不在json序列化结果当中
     * @return
     */
    @JsonIgnore
    public boolean isSuccess(){
        return Objects.equals(this.returnValue, BaseResponseCode.SUCCESS.getCode());
    }

    public Integer getReturnValue()
    {
        return returnValue;
    }

    public T getData() {
        return data;
    }

    public String getMsg() {
        return msg;
    }


    public static <T> ServerResponse<T> createBySuccess(){
        return new ServerResponse<T>(BaseResponseCode.SUCCESS.getCode());
    }

    public static <T> ServerResponse<T> createBySuccessMessage(String msg){
        return new ServerResponse<T>(BaseResponseCode.SUCCESS.getCode(),msg);
    }

    public static <T> ServerResponse<T> createBySuccess(T data){
        return new ServerResponse<T>(BaseResponseCode.SUCCESS.getCode(),data);
    }

    public static <T> ServerResponse<T> createBySuccess(String msg,T data){
        return new ServerResponse<T>(BaseResponseCode.SUCCESS.getCode(),msg,data);
    }

    public static <T> ServerResponse<T> createByError(){
        return new ServerResponse<T>(BaseResponseCode.SERVER_ERROR.getCode(),BaseResponseCode.SERVER_ERROR.getDesc());
    }

    public static <T> ServerResponse<T> createByErrorMessage(String errorMessage){
        return new ServerResponse<T>(BaseResponseCode.SERVER_ERROR.getCode(),errorMessage);
    }

    public static <T> ServerResponse<T> createByErrorCodeMessage(int errorCode,String errorMessage){
        return new ServerResponse<T>(errorCode,errorMessage);
    }

    public static <T> ServerResponse<T> createByErrorCodeMessageAndData(int errorCode,String errorMessage,T data){
        return new ServerResponse<T>(errorCode,errorMessage,data);
    }
    public static <T> ServerResponse<T> createByErrorCodeMessage(BaseResponseCode code) {
        return new ServerResponse<T>(code.getCode(),code.getDesc());
    }

    public static <T> ServerResponse<T>createParamError(){
        return new ServerResponse<T>(BaseResponseCode.ILLEGAL_ARGUMENT.getCode(),BaseResponseCode.ILLEGAL_ARGUMENT.getDesc());
    }



    @Override
    public String toString() {
        return "ServerResponse{" +
                "returnValue=" + returnValue +
                ", data=" + data +
                ", msg='" + msg + '\'' +
                '}';
    }


}
