package com.yufeng.yf.common.web.exception;

import com.yufeng.yf.common.web.entity.ServerResponse;
import com.yufeng.yf.common.web.enums.BaseResponseCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.NoHandlerFoundException;

import java.rmi.ServerException;
import java.util.HashMap;

/**
 * DATE:2017/5/1
 * TIME:上午11:04
 * @author gv
 */

@ControllerAdvice
public class ExceptionHandlerAdvice {

    private static Logger LOGGER = LoggerFactory.getLogger(ExceptionHandlerAdvice.class);

    /**
     * 拦截500异常，空指针异常
     * @param ex
     * @return
     */
    @ResponseBody
    @ExceptionHandler(value = NullPointerException.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public Object nullPointerException(Exception ex) {
        LOGGER.error("NullPointerException  >>> {}",ex);
        return ServerResponse.createByErrorMessage("空指针异常");
    }

    /**
     * 拦截处理404错误
     * @param ex
     * @return
     */
    @ExceptionHandler(value = NoHandlerFoundException.class)
    @ResponseBody
    public Object handleNoHandlerFoundException(NoHandlerFoundException ex) {
        LOGGER.error("404 >>> {}",ex);
        return ServerResponse.createByErrorCodeMessage(BaseResponseCode.TOKEN_NULL.getCode(), "您访问对页面不存在");
    }

    /**
     * 业务异常
     * @param ex
     * @return
     */
    @ExceptionHandler(value = ServerException.class)
    @ResponseBody
    public Object serviceException(ServerException ex) {
        LOGGER.error("service exception :{}",ex.getMessage());
        return ServerResponse.createByErrorMessage(ex.getMessage());
    }

    /**
     * 业务异常
     * @param ex
     * @return
     */
    @ExceptionHandler(value = ParamException.class)
    @ResponseBody
    public Object paramException(ParamException ex) {
        LOGGER.error("param exception :{}",ex.getMessage());
        return ServerResponse.createByErrorCodeMessage(BaseResponseCode.ILLEGAL_ARGUMENT.getCode(),ex.getMessage());
    }
    /**
     * 乐观锁异常
     * @param ex
     * @return
     */
    @ExceptionHandler(value = OptimisticLockException.class)
    @ResponseBody
    public Object optimisticLockException(OptimisticLockException ex) {
        LOGGER.error("optimistic Lock exception :{}",ex.getMessage());
        return ServerResponse.createByErrorCodeMessage(BaseResponseCode.OPERATION_TOO_FREQUENT);
    }


    /**
     * 拦截处理token未传递异常
     *
     * @param ex
     * @return
     */
    @ExceptionHandler(value = TokenNullException.class)
    @ResponseBody
    public Object tokenNullException(TokenNullException ex) {
        LOGGER.error(" request url :[{}]  token null exception", ex.getMessage());
        return ServerResponse.createByErrorCodeMessage(BaseResponseCode.TOKEN_NULL.getCode(), BaseResponseCode.TOKEN_NULL.getDesc());
    }

    /**
     * 拦截处理token未传递异常
     *
     * @param ex
     * @return
     */
    @ExceptionHandler(value = TokenErrorException.class)
    @ResponseBody
    public Object tokenErrorException(TokenErrorException ex) {
        LOGGER.error("request url :[{}] token error exception", ex.getMessage());
        return ServerResponse.createByErrorCodeMessage(BaseResponseCode.TOKEN_ERROR.getCode(), BaseResponseCode.TOKEN_ERROR.getDesc());
    }

    /**
     * 版本未找到异常
     * @param ex
     * @return
     */
    @ExceptionHandler(value = VersionNotFoundException.class)
    @ResponseBody
    public Object versionNotFoundException(VersionNotFoundException ex) {
        LOGGER.error("version not found exception", ex.getMessage());
        return ServerResponse.createByErrorCodeMessage(BaseResponseCode.VERSION_NOT_FOUND.getCode(), BaseResponseCode.VERSION_NOT_FOUND.getDesc());
    }

    /**
     * 版本访问域名不匹配
     * @param ex
     * @return
     */
    @ExceptionHandler(value = VisitException.class)
    @ResponseBody
    public Object visitException(VisitException ex) {
        LOGGER.error("version visit exception", ex.getMessage());
        HashMap map=new HashMap();
        map.put("clientCurrentVersionStatus",ex
                .getClientCurrentVersionStatus());
        return ServerResponse.createByErrorCodeMessageAndData(BaseResponseCode.VERSION_DOMAIN_ERROR.getCode(), BaseResponseCode.VERSION_DOMAIN_ERROR.getDesc(),map);
    }


    /**
     * @param ex
     * @return
     */
    @ExceptionHandler(value = HttpRequestMethodNotSupportedException.class)
    @ResponseBody
    public Object methodNotSupported(Exception ex) {
        LOGGER.error("{}",ex);
        return ServerResponse.createByErrorCodeMessage(BaseResponseCode.ILLEGAL_ARGUMENT.getCode(),ex.getMessage());
    }

    /**
     * @param ex
     * @return
     */
    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public Object exception(Exception ex) {
        LOGGER.error("{}",ex);
        return ServerResponse.createByError();
    }



}
