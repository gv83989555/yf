package com.yufeng.yf.common.web.exception;

/**
 * @author GV
 * Date:2018/6/25
 * Time:下午2:32
 */

public class OptimisticLockException extends RuntimeException {

    public OptimisticLockException(String message) {
        super(message);
    }
}
