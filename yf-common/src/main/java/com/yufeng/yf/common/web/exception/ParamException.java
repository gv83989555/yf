package com.yufeng.yf.common.web.exception;

/**
 * @author GV
 * Date:2018/4/15
 * Time:下午9:47
 */

public class ParamException extends RuntimeException {

    public ParamException(String message) {
        super(message);
    }

    public ParamException() {

    }
}
