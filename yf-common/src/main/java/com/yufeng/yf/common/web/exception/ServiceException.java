package com.yufeng.yf.common.web.exception;

/**
 * @author GV
 * Date:2018/4/15
 * Time:下午9:47
 */

public class ServiceException extends RuntimeException {

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException() {

    }
}
