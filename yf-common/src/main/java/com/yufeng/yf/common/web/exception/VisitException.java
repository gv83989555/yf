package com.yufeng.yf.common.web.exception;

/**
 * @author GV
 * Date:2018/8/01
 * Time:下午15:22
 */

public class VisitException extends Exception {

    private Integer clientCurrentVersionStatus;
    public VisitException(String message) {
        super(message);
    }

    public VisitException(Integer clientCurrentVersionStatus){
        super();
        this.clientCurrentVersionStatus=clientCurrentVersionStatus;
    }

    public Integer getClientCurrentVersionStatus() {
        return clientCurrentVersionStatus;
    }

    public void setClientCurrentVersionStatus(Integer clientCurrentVersionStatus) {
        this.clientCurrentVersionStatus = clientCurrentVersionStatus;
    }
}
