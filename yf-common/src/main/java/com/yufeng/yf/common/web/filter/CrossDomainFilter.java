package com.yufeng.yf.common.web.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * DATE:2017/6/13
 * TIME:下午2:01
 * ajax跨域访问过滤器
 *
 * @author gv
 */
@WebFilter(filterName = "CrossDomainFilter", urlPatterns = {"*"})
@Component
public class CrossDomainFilter implements Filter {

    private static final Logger log = LoggerFactory.getLogger(CrossDomainFilter.class);

    public CrossDomainFilter() {

    }

    @Override
    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletResponse httpServletResponse = (HttpServletResponse) resp;
        HttpServletRequest httpServletRequest = (HttpServletRequest) req;
        String curOrigin = httpServletRequest.getHeader("Origin");
//        log.info("curOrigin:{}",curOrigin);
        httpServletResponse.setHeader("Access-Control-Allow-Origin", curOrigin);
        httpServletResponse.setHeader("Access-Control-Allow-Headers", "Origin,X-Requested-With,Content-Type,Accept,token");
        httpServletResponse.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT,HEAD");
        httpServletResponse.setHeader("Access-Control-Max-Age", "3600");
        //是否支持cookie跨域
        httpServletResponse.setHeader("Access-Control-Allow-Credentials", "true");
        chain.doFilter(req, resp);
    }

    @Override
    public void init(FilterConfig config) throws ServletException {
        log.info(" {CrossDomainFilter} inited");
    }

}
