package com.yufeng.yf.common.web.validator;

import com.baidu.unbiz.fluentvalidator.ValidationError;
import com.baidu.unbiz.fluentvalidator.Validator;
import com.baidu.unbiz.fluentvalidator.ValidatorContext;
import com.baidu.unbiz.fluentvalidator.ValidatorHandler;


/**
 * @author GV
 * Date:2018/4/13
 * Time:上午10:22
 */

public class ObjectNotNullValidator  extends ValidatorHandler<Object> implements Validator<Object> {


    private String fieldName;

    public ObjectNotNullValidator(String fieldName) {
        this.fieldName = fieldName;
    }

    @Override
    public boolean validate(ValidatorContext context, Object s) {
        if (null == s) {
            context.addError(ValidationError.create(String.format("%s不能为null！", fieldName))
                    .setErrorCode(-1)
                    .setField(fieldName)
                    .setInvalidValue(s));
            return false;
        }
        return true;
    }
}
